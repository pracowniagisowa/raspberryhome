using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using RaspberryHome_App.SideServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace PASZO_Core.SideServices
{
    public class TimeTicker : HostedService
    {
        private readonly TimeScheduler _timeScheduler;
        private readonly PinReader _pinReader;
        private IConfiguration _configuration;
        private readonly ILogger<TimeTicker> _logger;

        private DateTime _lastLoggingTime = DateTime.MinValue;
        private DateTime _lastPeriodAction = DateTime.MinValue;


        public TimeTicker(TimeScheduler timeScheduler, PinReader pinReader, ILogger<TimeTicker> logger)
            : base(logger)
        {
            _timeScheduler = timeScheduler;
            _pinReader = pinReader;
            _logger = logger;
        }

        protected override async Task ExecuteAsync(CancellationToken cancellationToken)
        {
            var machineName = Environment.MachineName;
            while (!cancellationToken.IsCancellationRequested)
            {
                var nowUtc = DateTime.UtcNow;
                if (nowUtc - _lastLoggingTime > new TimeSpan(1, 0, 0))
                {
                    _logger.LogInformation("TickTock!");
                    _lastLoggingTime = nowUtc;

                }

                try
                {
                    await Task.Delay(TimeSpan.FromSeconds(0.5), cancellationToken);
                    // _logger.LogInformation("Log every time!");
                    _pinReader.WatchPin();
                    // 
                    if (nowUtc - _lastPeriodAction > new TimeSpan(0, 0, 5))
                    {
                        await _timeScheduler.PeriodActions();
                        // _logger.LogInformation("PeriodActions!");
                        _lastPeriodAction = nowUtc;
                    }

                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, "TimeTicker error...");
                }

            }

        }
    }
}
