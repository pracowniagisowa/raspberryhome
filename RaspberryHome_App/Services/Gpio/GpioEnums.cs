namespace RaspberryHome_App.Services
{
    public enum GpioPinMode
    {
        //
        // Summary:
        //     Input used for reading values from a pin.
        Input = 0,
        //
        // Summary:
        //     Output used for writing values to a pin.
        Output = 1,
    }

    public enum GpioPinValue
    {
        Low = 0,
        High = 1
    }
}