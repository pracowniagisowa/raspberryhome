export class PinFromDB {
  comment: string;
  gpioStateId: number;
  isOpen: boolean;
  isStateHigh: boolean;
  pinMode: number;
  pinNo: number;
  pinPhisicalNo: number;
}
