namespace RPI_GPIO_Tests
{
    public interface IGpioHelper
    {
        void SetPinState(int pinId, int pinValue, bool saveToDb = true);
    }
}