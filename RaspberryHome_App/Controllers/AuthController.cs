using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using RaspberryHome_App.DatabaseModels.Identity;
using RaspberryHome_App.DTOs;
using RaspberryHome_App.Models.Auth;
using RaspberryHome_App.Settings;

namespace RaspberryHome_App.Controllers
{
    [ApiController]
    [Route("[controller]/[action]")]
    public class AuthController : ControllerBase
    {
        private IMapper _mapper;
        private UserManager<RaspberryHomeUser> _userManager;
        private RoleManager<Role> _roleManager;
        private JwtSettings _jwtSettings;

        public AuthController(
            IMapper mapper,
            UserManager<RaspberryHomeUser> userManager,
            RoleManager<Role> roleManager,
            IOptionsSnapshot<JwtSettings> jwtSettings)
        {
            _mapper = mapper;
            _userManager = userManager;
            _roleManager = roleManager;
            _jwtSettings = jwtSettings.Value;


        }

        [HttpPost]
        public async Task<IActionResult> SignUp(Credentials credentials)
        {
            var user = new RaspberryHomeUser();
            user.UserName = credentials.UserName;



            var userCreateResult = await _userManager.CreateAsync(user, credentials.Password);

            if (userCreateResult.Succeeded)
            {
                return Created(string.Empty, string.Empty);
            }

            return Problem(userCreateResult.Errors.First().Description, null, 500);
        }

        [HttpPost]
        public async Task<IActionResult> SignIn(Credentials credentials)

        {
            var response = new SignInResponse();

            var user = _userManager.Users.SingleOrDefault(u => u.UserName == credentials.UserName);
            if (user is null)
            {
                return NotFound("User not found");
            }

            var userSigninResult = await _userManager.CheckPasswordAsync(user, credentials.Password);


            if (userSigninResult)
            {
                var roles = await _userManager.GetRolesAsync(user);
                var token = GenerateJwt(user, roles);
                response.IsAuthorized = true;
                response.Token = token;

                return Ok(response);

            }
            else
            {
                return Ok(response);
            }
        }

        [Authorize]
        [HttpGet]
        public async Task<ActionResult> GetUser()
        {
            ClaimsPrincipal currentUser = this.User;
            var currentUserID = currentUser.FindFirst(ClaimTypes.NameIdentifier).Value;
            RaspberryHomeUser user = await _userManager.FindByIdAsync(currentUserID);
            RaspberryHomeUserDTO userDTO = _mapper.Map<RaspberryHomeUser, RaspberryHomeUserDTO>(user);

            return Ok(userDTO);
        }


        #region Private Methods

        private string GenerateJwt(RaspberryHomeUser user, IList<string> roles)
        {
            var claims = new List<Claim>
            {
                new Claim(JwtRegisteredClaimNames.Sub, user.Id.ToString()),
                new Claim(ClaimTypes.Name, user.UserName),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                new Claim(ClaimTypes.NameIdentifier, user.Id.ToString())
            };

            var roleClaims = roles.Select(r => new Claim(ClaimTypes.Role, r));
            claims.AddRange(roleClaims);

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_jwtSettings.Secret));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            var expires = DateTime.Now.AddDays(Convert.ToDouble(_jwtSettings.ExpirationInDays));

            var token = new JwtSecurityToken(
                issuer: _jwtSettings.Issuer,
                audience: _jwtSettings.Issuer,
                claims,
                expires: expires,
                signingCredentials: creds
            );

            return new JwtSecurityTokenHandler().WriteToken(token);
        }

        #endregion


    }
}