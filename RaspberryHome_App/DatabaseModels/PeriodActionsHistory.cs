using System;
using System.ComponentModel.DataAnnotations;

namespace RaspberryHome_App.DatabaseModels
{
    public class PeriodActionHistory
    {
        [Key]
        public int PeriodActionHistoryId { get; set; }
        public string TechnicalName { get; set; }
        public DateTime InitTime { get; set; }
        public DateTime EndTime { get; set; }
        public bool IsActionTriggered { get; set; }
        public bool IsCompleted { get; set; }
        public string Comment { get; set; }
    }
}