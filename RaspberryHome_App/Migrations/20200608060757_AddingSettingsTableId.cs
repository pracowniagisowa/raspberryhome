﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace raspberryhome.Migrations
{
    public partial class AddingSettingsTableId : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "SettingsDb",
                columns: table => new
                {
                    SettingDbId = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    SettingKey = table.Column<string>(nullable: true),
                    StringValue = table.Column<string>(nullable: true),
                    IntValue = table.Column<int>(nullable: false),
                    BoolValue = table.Column<bool>(nullable: false),
                    FloatValue = table.Column<float>(nullable: false),
                    DoubleValue = table.Column<double>(nullable: false),
                    DecimalValue = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SettingsDb", x => x.SettingDbId);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "SettingsDb");
        }
    }
}
