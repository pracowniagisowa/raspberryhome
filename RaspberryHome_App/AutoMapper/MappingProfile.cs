using AutoMapper;
using RaspberryHome_App.DatabaseModels.Identity;
using RaspberryHome_App.DTOs;

namespace RaspberryHome_App.AutoMapper
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<RaspberryHomeUser, RaspberryHomeUserDTO>();
        }

    }
}