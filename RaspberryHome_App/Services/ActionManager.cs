using System.Collections.Generic;
using System.Linq;
using RaspberryHome_App.DatabaseModels;
using RaspberryHome_App.Services.Garden;

namespace RaspberryHome_App.Services
{
    public class ActionManager
    {
        public static List<PeriodAction> PeriodActions = new List<PeriodAction>{
            new PeriodAction("DryHomePlantsNotifireAction", "Powiadomienie czy nie za sucho na parapecie", new DryHomePlantsNotifireAction()),
            new PeriodAction("WaterPlantsAction", "Podlewanie roślin na balkonie", new WaterPlantsAction()),
            new PeriodAction("SoilHumidityToDbAction", "Wilgotność roślin na baloknie do bazy", new SoilHumidityToDbAction()),
            new PeriodAction("TomorrowFrostNotifireAction", "Powiadomienie czy będą jutro przymrozki", new TomorrowFrostNotifireAction())
        };

        public static void AddActionToDbWhenMissing(HomeDbContext dbContext)
        {
            foreach (var periodAction in PeriodActions)
            {
                PeriodAction action = dbContext.PeriodActions.FirstOrDefault(x => x.TechnicalName == periodAction.TechnicalName);
                if (action == null)
                {
                    PeriodAction newAction = new PeriodAction();
                    newAction.TechnicalName = periodAction.TechnicalName;
                    newAction.Description = periodAction.Description;
                    newAction.ActionFrequency = new ActionFrequency();

                    dbContext.PeriodActions.Add(newAction);
                    dbContext.SaveChanges();
                }
            }
        }
    }
}