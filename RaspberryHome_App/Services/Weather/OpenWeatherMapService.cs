using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using RaspberryHome_App.Services.Weather.Classes;
using RestSharp;

namespace RaspberryHome_App.Services.Weather
{
    public class OpenWeatherMapService
    {
        private string _appId = "648d45013a4056a0e32f9a7168b85c8f";
        private int _cityId = 3094802;
        private string _units = "metric";
        private string _uri = "http://api.openweathermap.org/data/2.5/";

        private IRestClient _client;

        public OpenWeatherMapService()
        {
            _client = new RestClient(_uri);
        }

        public List<FrostInADay> TomorrowFrost()
        {
            Root forecast = GetWeatherForecast().Result;

            DateTime today = DateTime.Now.Date;
            DateTime tomorrow = today.AddDays(1);
            List<FrostInADay> frostDict = new List<FrostInADay>();

            foreach (var item in forecast.list)
            {
                long epochSeconds = item.dt;
                DateTimeOffset forecastDateTimeOffset = DateTimeOffset.FromUnixTimeSeconds(epochSeconds);
                DateTime forecastDateTime = forecastDateTimeOffset.LocalDateTime;

                if (forecastDateTime.Date == tomorrow && item.main.temp_min <= 10)
                {
                    frostDict.Add(new FrostInADay(forecastDateTime, item.main.temp_min));
                }
            }

            return frostDict;
        }

        private async Task<Root> GetWeatherForecast()
        {
            RestRequest request = new RestRequest("forecast", Method.GET);
            request.AddQueryParameter("id", _cityId.ToString());
            request.AddQueryParameter("appid", _appId);
            request.AddQueryParameter("units", _units);

            var response = await _client.GetAsync<Root>(request);
            return response;
        }

    }
}