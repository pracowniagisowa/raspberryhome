using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using RaspberryHome_App.DatabaseModels;
using RaspberryHome_App.Services;
using RaspberryHome_App.Services.Home;

namespace RaspberryHome_App.Controllers
{
    [ApiController]
    [Route("[controller]/[action]")]
    public class HomeController : ControllerBase
    {
        private readonly HomeDbContext _dbContext;
        private readonly IConfiguration _configuration;
        private IGpioManager _gpioManager;
        private readonly ILogger _logger;
        private HomeService _service;

        public HomeController(HomeDbContext dbContext, IGpioManager gpioManager, IConfiguration configuration, ILogger<HomeController> logger)
        {
            _dbContext = dbContext;
            _configuration = configuration;
            _logger = logger;
            _gpioManager = gpioManager;
            _service = new HomeService(_dbContext);
        }

        [HttpGet]
        public async Task<ActionResult> GetThermometesFullInfo()
        {
            var thermsFullInfos = await _service.GetThermometerFullInfos();
            return Ok(thermsFullInfos);
        }

        [HttpGet]
        public async Task<ActionResult> GetTemperature(string thermId)
        {
            var temperature = await _service.GetTemperature(thermId);
            return Ok(temperature);
        }

        [HttpGet]
        public ActionResult GetThermoemters()
        {
            var therms = _service.GetThermometers();

            return Ok(therms);
        }

        [HttpPut]
        public ActionResult PutThermometerNameByThermometerID([FromQuery] string thermId, [FromQuery] string name){
            var thermFullInfo = _dbContext.ThermometerFullInfos.FirstOrDefault(x => x.ThermometerID == thermId);
            thermFullInfo.Name = name;
            _dbContext.SaveChanges();

            return Ok();
        }
    }
}