import React from "react";

import {
  BrowserRouter as Router,
  Switch,
  Route,
  useParams,
  Link,
  BrowserRouter,
} from "react-router-dom";

import Home from "./DashboardComponents/HomeDashboard";
import GpioDashboard from "./DashboardComponents/GpioDashboard";
import { AppPaths } from "../AppConstans";

export interface DashboardProps { }

export interface DashboardState { }

class Dashboard extends React.Component<DashboardProps, DashboardState> {
  constructor(props: DashboardProps) {
    super(props);
    this.state = {};
  }
  render() {
    return (
      <div>
        <div className="full-size">
          <Switch>
            <Route exact path={AppPaths.HomePath} component={Home} />
            <Route path={AppPaths.GpioPath} component={GpioDashboard} />
          </Switch>
        </div>
      </div>
    );
  }
}

export default Dashboard;
