using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using RaspberryHome_App.DatabaseModels;

namespace RaspberryHome_App.Services
{
	public class SettingsService
	{
		private readonly HomeDbContext _homeDbContext;


		public SettingsService(HomeDbContext HomeDbContext)
		{
			_homeDbContext = HomeDbContext;
		}

		#region BoolValue

		public bool GetBoolValue(string key)
		{
			var setting = _homeDbContext.SettingsDb.FirstOrDefault(s => s.SettingKey == key);
			if (setting != null)
			{
				return setting.BoolValue;
			}
			return false;
		}

		public void SetBoolValue(string key, bool value)
		{
			var setting = _homeDbContext.SettingsDb.FirstOrDefault(s => s.SettingKey == key);
			if (setting != null)
			{
				setting.BoolValue = value;
			}
			else
			{
				setting = new SettingDb(key, value);
				_homeDbContext.SettingsDb.Add(setting);                
			}
			_homeDbContext.SaveChanges();
		}

		#endregion

		#region IntValue

		public void SetIntValue(string key, int value)
		{
			var setting = _homeDbContext.SettingsDb.FirstOrDefault(s => s.SettingKey == key);
			if (setting != null) {
				setting.IntValue = value;
			} else {
				setting = new SettingDb(key, value);
				_homeDbContext.SettingsDb.Add(setting);
			}
			_homeDbContext.SaveChanges();
		}

		public int GetIntValue(string key)
		{
			var setting = _homeDbContext.SettingsDb.FirstOrDefault(s => s.SettingKey == key);
			if (setting != null) {
				return setting.IntValue;
			}
			return 0;
		}

		#endregion

		#region StringValue

		public string GetStringValue(string key)
		{
			var setting = _homeDbContext.SettingsDb.FirstOrDefault(s => s.SettingKey == key);
			if (setting != null)
			{
				return setting.StringValue;
			}
			return String.Empty;
		}



		#endregion

		#region FloatValue
		public void SetFloatValue(string key, float value)
		{
			var setting = _homeDbContext.SettingsDb.FirstOrDefault(s => s.SettingKey == key);
			if (setting != null) {
				setting.FloatValue = value;
			} else {
				setting = new SettingDb(key, value);
				_homeDbContext.SettingsDb.Add(setting);
			}
			_homeDbContext.SaveChanges();
		}

		public float GetFloatValue(string key)
		{
			var setting = _homeDbContext.SettingsDb.FirstOrDefault(s => s.SettingKey == key);
			if (setting != null) {
				return setting.FloatValue;
			}
			return 0;
		}

		#endregion

		#region DoubleValue
		public void SetDoubleValue(string key, double value)
		{
			var setting = _homeDbContext.SettingsDb.FirstOrDefault(s => s.SettingKey == key);
			if (setting != null) {
				setting.DoubleValue = value;
			} else {
				setting = new SettingDb(key, value);
				_homeDbContext.SettingsDb.Add(setting);
			}
			_homeDbContext.SaveChanges();
		}

		public double GetDoubleValue(string key)
		{
			var setting = _homeDbContext.SettingsDb.FirstOrDefault(s => s.SettingKey == key);
			if (setting != null) {
				return setting.DoubleValue;
			}
			return 0;
		}

		#endregion

		#region DecimalValue
		public void SetDecimalValue(string key, decimal value)
		{
			var setting = _homeDbContext.SettingsDb.FirstOrDefault(s => s.SettingKey == key);
			if (setting != null) {
				setting.DecimalValue = value;
			} else {
				setting = new SettingDb(key, value);
				_homeDbContext.SettingsDb.Add(setting);
			}
			_homeDbContext.SaveChanges();
		}

		public decimal GetDecimalValue(string key)
		{
			var setting = _homeDbContext.SettingsDb.FirstOrDefault(s => s.SettingKey == key);
			if (setting != null) {
				return setting.DecimalValue;
			}
			return 0;
		}

		#endregion

	}
}
