using System.ComponentModel.DataAnnotations;

namespace RaspberryHome_App.DatabaseModels
{
    public class SettingDb
    {
		[Key]
        public int SettingDbId { get; set; }
		public string SettingKey { get; set; }
		public string StringValue { get; set; }
		public int IntValue { get; set; }
		public bool BoolValue { get; set; }
		public float FloatValue { get; set; }
		public double DoubleValue { get; set; }
		public decimal DecimalValue { get; set; }

		#region Constructors

		private SettingDb(string settingKey)
		{
			SettingKey = settingKey;
		}

		public SettingDb(string settingKey, string stringValue)
			:this(settingKey)
		{
			StringValue = stringValue;
		}

		public SettingDb(string settingKey, int intValue)
			:this(settingKey)
		{
			IntValue = intValue;
		}

		public SettingDb(string settingKey, bool boolValue)
			:this(settingKey)
		{
			BoolValue = boolValue;
		}

		public SettingDb(string settingKey, float floatValue)
			:this(settingKey)
		{
			FloatValue = floatValue;
		}

		public SettingDb(string settingKey, double doubleValue)
			:this(settingKey)
		{
			DoubleValue = doubleValue;
		}

		public SettingDb(string settingKey, decimal decimalValue)
			:this(settingKey)
		{
			DecimalValue = decimalValue;
		}

		#endregion
    }
}