using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using RaspberryHome_App.DatabaseModels;
using RaspberryHome_App.SideServices;
using RaspberryHome_App.SideServices.Classes;

namespace RaspberryHome_App.Services.Garden
{
    public class WaterPlantsAction : BaseAction
    {
        // private readonly GardenManager _gardenManager;
        public WaterPlantsAction()
        {
        }

        public WaterPlantsAction(IServiceScopeFactory scopeFactory, ILogger logger)
            : base(scopeFactory, logger)
        {
            // _gardenManager = gardenManager;
        }

        public override ActionResponse Action()
        {
            ActionResponse response = new ActionResponse();
            using (var scope = _scopeFactory.CreateScope())
            {
                var gardenManager = scope.ServiceProvider.GetRequiredService<GardenManager>();
                if (!gardenManager.IsSoilHumid((int)GpioPinsUsage.SoilHumidityBalconySensorValue, (int)GpioPinsUsage.SoilHumidityBalconySensorPower))
                {
                    response.Triggered = true;
                    gardenManager.WaterPlants((int)GpioPinsUsage.WaterPumpSwitch);
                    response.Success = true;
                }
            }

            return response;
        }
    }

}