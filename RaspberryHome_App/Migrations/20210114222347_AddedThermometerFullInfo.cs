﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace raspberryhome.Migrations
{
    public partial class AddedThermometerFullInfo : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ThermometerFullInfos",
                columns: table => new
                {
                    ThermometerFullInfoID = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    ThermometerID = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ThermometerFullInfos", x => x.ThermometerFullInfoID);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ThermometerFullInfos");
        }
    }
}
