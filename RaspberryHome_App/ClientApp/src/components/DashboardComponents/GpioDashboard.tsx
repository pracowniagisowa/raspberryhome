import React from "react";
import InputGroup from "react-bootstrap/InputGroup";
import FormControl from "react-bootstrap/FormControl";
import Button from "react-bootstrap/Button";
import Badge from "react-bootstrap/Badge";
import { AppKey, AppPaths, AppUrl } from "../../AppConstans";
import { PinFromDB } from "../../classes/PinFromDB";
import { IToken } from "../../classes/IToken";
import Cookies from "universal-cookie";
import { Typography } from "@material-ui/core";
import GpioPinStateComponent from "./GpioPinStateComponent";

export interface GpioDashboardProps { }

export interface GpioDashboardState {
	switchPinNo: number;
	switchPinState: string;
	getPinValueNo: number;
	pinStates: PinFromDB[];
}

class GpioDashboard extends React.Component<GpioDashboardProps, GpioDashboardState> {

	public cookies: Cookies;
	public token: string;

	constructor(props: GpioDashboardProps) {
		super(props);
		this.state = {
			switchPinNo: 2,
			switchPinState: "",
			getPinValueNo: 0,
			pinStates: new Array<PinFromDB>(),
		};

		this.cookies = new Cookies();
		this.token = this.cookies.get(AppKey.cookieToken);

		this.switchPinHandleChange = this.switchPinHandleChange.bind(this);
		this.switchPinClickHandleChange = this.switchPinClickHandleChange.bind(this);
	}

	componentDidMount() {
		this.token = this.cookies.get(AppKey.cookieToken);
		this.updateGpioFromDB();
	}

	render() {
		return (
			<React.Fragment>
				<div className="container-fluid">
					<br />
					<Typography>
						GPIO Dashboard
					</Typography>
					<br />
					{this.state.pinStates.map(x =>
						<GpioPinStateComponent
							pinFromDB={x}
							token={this.token}
							onPinsFromDbRefresh={this.handlePinsFromDbRefresh} />)
					}


				</div>
			</React.Fragment>
		);
	}

	updateGpioFromDB() {
		fetch(AppUrl.GetPinsFromDb,
			{
				method: "GET",
				headers: new Headers({
					Authorization:
						"Bearer " + this.token
				}),
			})
			.then(resp => resp.json())
			.then(json => this.setState({ pinStates: json }))
	}

	displayStateBadge(state: string) {
		let badge: any;
		switch (state) {
			case "Low":
				badge = <Badge variant="warning">Low</Badge>;
				break;
			case "High":
				badge = <Badge variant="primary">High</Badge>;
				break;
			default:
				break;
		}
		return badge;
	}

	switchPinHandleChange(event: any) {
		console.log(event.target.value);
		if (event.target.value <= 27 && event.target.value >= 0) {
			this.setState({ switchPinNo: event.target.value });
		}
	}

	switchPinClickHandleChange(event: any) {
		fetch("gpio/SwichPinValue?pinID=" + this.state.switchPinNo)
			.then((res) => res.json())
			.then((json) => this.setState({ switchPinState: json.message }))
			.catch((err) => alert(err));
	}

	handlePinsFromDbRefresh = () => {
		this.updateGpioFromDB();
	}
}

export default GpioDashboard;
