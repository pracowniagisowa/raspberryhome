using System.ComponentModel.DataAnnotations;

namespace RaspberryHome_App.DatabaseModels
{
    public class TestEntity
    {
        [Key]
        public int TestEntityId { get; set; }
        public string Comment { get; set; }
    }
}