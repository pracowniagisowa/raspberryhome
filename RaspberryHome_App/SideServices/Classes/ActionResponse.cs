using System;

namespace RaspberryHome_App.SideServices.Classes
{
    public class ActionResponse
    {
        public Guid ActionID { get; set; }
        public bool Triggered { get; set; }
        public bool Success { get; set; }
        public string Message { get; set; }
    }
}