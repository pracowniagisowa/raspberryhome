﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace raspberryhome.Migrations
{
    public partial class Actions : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "EndTime",
                table: "PeriodActionHistories",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<bool>(
                name: "IsCompleted",
                table: "PeriodActionHistories",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "EndTime",
                table: "PeriodActionHistories");

            migrationBuilder.DropColumn(
                name: "IsCompleted",
                table: "PeriodActionHistories");
        }
    }
}
