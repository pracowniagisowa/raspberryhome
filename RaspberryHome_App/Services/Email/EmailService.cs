using System.Net;
using MailKit.Net.Smtp;
using MimeKit;
using MimeKit.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using RaspberryHome_App.DatabaseModels.Identity;
using System.Net.Mail;

namespace RaspberryHome_App.Services
{
    public class EmailService
    {
        private UserManager<RaspberryHomeUser> _userManager;
        private readonly IConfiguration _configuration;
        // private string _emailAdress;

        public EmailService(UserManager<RaspberryHomeUser> userManager, IConfiguration configuration)
        {
            _userManager = userManager;
            _configuration = configuration;
        }

        public async Task SendEmail(string emailAdress, string subject, string body, bool isBodyHtml)
        {
            using (MailMessage message = new MailMessage())
            {
                message.To.Add(emailAdress);
                message.From = new MailAddress("lokietka.home@gmail.com");
                message.Subject = subject;
                message.Body = body;
                message.IsBodyHtml = isBodyHtml;

                using (System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient())
                {
                    smtp.Host = "smtp.gmail.com";
                    smtp.EnableSsl = true;
                    NetworkCredential cred = new NetworkCredential(
                       userName: "lokietka.home@gmail.com",
                       password: "qzqxjxqjblkvwxjn");
                    smtp.UseDefaultCredentials = false;

                    smtp.Credentials = cred;
                    smtp.Port = 587;
                    smtp.Send(message);
                }
            }
        }

        private async Task<string> GetAdressFrom()
        {
            string powerUserUserName = _configuration.GetSection("PowerUserSettings:Login").Value;
            var powerUser = await _userManager.FindByNameAsync(powerUserUserName);
            return await _userManager.GetEmailAsync(powerUser);
        }
    }
}


// using (var message = new MimeMessage())
// {
//     var fromEmailAdress = await GetAdressFrom();

//     message.From.Add(MailboxAddress.Parse(fromEmailAdress));// = new MailAddress(fromEmailAdress, "Raspberry Home");
//     message.To.Add(MailboxAddress.Parse("maciek.fertala@gmail.com"));
//     message.Subject = subject;
//     message.Body = new TextPart(TextFormat.Plain) { Text = body };

//     using (var client = new SmtpClient())
//     {
//         //gmail
//         client.Connect("smtp.gmail.com", 587, MailKit.Security.SecureSocketOptions.StartTls);
//         client.AuthenticationMechanisms.Remove("XOAUTH2");
//         client.Authenticate(fromEmailAdress, "Razdwatrzy4");
//         // client.EnableSsl = true;
//         client.Send(message);
//         client.Disconnect(true);
//     }
// }