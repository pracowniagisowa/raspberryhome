using System;
using System.Collections.Generic;
using System.Device.Gpio;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.SpaServices.ReactDevelopmentServer;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using PASZO_Core.SideServices;
using RaspberryHome_App.AutoMapper;
using RaspberryHome_App.DatabaseModels;
using RaspberryHome_App.DatabaseModels.Identity;
using RaspberryHome_App.Extensions;
using RaspberryHome_App.Models.Auth;
using RaspberryHome_App.Services;
using RaspberryHome_App.Services.Garden;
using RaspberryHome_App.Services.Home;
using RaspberryHome_App.Settings;
using RaspberryHome_App.SideServices;

namespace raspberryhome
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<HomeDbContext>(
                o => o.UseSqlite(
                    Configuration.GetConnectionString("RaspberryHomeServerConnection")
                ));

            services.AddIdentity<RaspberryHomeUser, Role>(o =>
            {
                // o.SignIn.RequireConfirmedAccount = true;
                o.Password.RequiredLength = 1;
                o.Password.RequireUppercase = false;
                o.Password.RequireNonAlphanumeric = false;
            })
                .AddRoles<Role>()
                .AddEntityFrameworkStores<HomeDbContext>()
                .AddDefaultTokenProviders();

            services.Configure<JwtSettings>(Configuration.GetSection("Jwt"));
            var jwtSettings = Configuration.GetSection("Jwt").Get<JwtSettings>();
            services.AddAuth(jwtSettings);

            // / Auto Mapper Configurations
            var mapperConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new MappingProfile());
            });

            IMapper mapper = mapperConfig.CreateMapper();
            services.AddSingleton(mapper);

            services.AddControllersWithViews();

            // In production, the React files will be served from this directory
            services.AddSpaStaticFiles(configuration =>
            {
                configuration.RootPath = "ClientApp/build";
            });
            services.AddSwaggerGen();

            if (Environment.MachineName == "raspberryhome")
            {
                services.AddScoped<IGpioManager, PiGpioManager>();
            }
            else
            {
                services.AddScoped<IGpioManager, GpioMockManager>();
            }

            // services.AddSingleton<TaskManager>();

            services.AddScoped<EmailService>();
            services.AddScoped<GardenManager>();
            services.AddScoped<HomeService>();

            services.AddSingleton<TimeScheduler>();
            services.AddSingleton<PinReader>();
            services.AddSingleton<IHostedService, TimeTicker>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(
            IApplicationBuilder app,
            IWebHostEnvironment env,
            HomeDbContext dbContext,
            IGpioManager gpioManager,
            IServiceProvider serviceProvider,
            HomeService homeService)

        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                // app.UseMigrationsEndPoint();
            }
            else
            {
                app.UseExceptionHandler("/Error");
            }

            // app.UseExceptionHandler("/Error");

            app.UseStaticFiles();

            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Raspberry Home");
            });
            app.UseSpaStaticFiles();
            app.UseRouting();
            app.UseAuth();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller}/{action=Index}/{id?}");
            });

            app.UseSpa(spa =>
            {
                spa.Options.SourcePath = "ClientApp";

                if (env.IsDevelopment())
                {
                    spa.UseReactDevelopmentServer(npmScript: "start");
                }
            });

            dbContext.Database.Migrate();
            CreateRoles(serviceProvider).GetAwaiter().GetResult();
            gpioManager.InitGpio();
            homeService.UpdateThermometersListInDatabase();
            ActionManager.AddActionToDbWhenMissing(dbContext);

        }

        private async Task CreateRoles(IServiceProvider serviceProvider)
        {
            //adding custom roles
            var roleManager = serviceProvider.GetRequiredService<RoleManager<Role>>();
            var userManager = serviceProvider.GetRequiredService<UserManager<RaspberryHomeUser>>();
            var roleNames = Configuration.GetSection("UserRoles").Get<List<string>>();
            IdentityResult roleResult;

            foreach (var roleName in roleNames)
            {
                //creating the roles and seeding them to the database
                var roleExist = await roleManager.RoleExistsAsync(roleName);
                if (!roleExist)
                {
                    roleResult = await roleManager.CreateAsync(new Role(roleName));
                }
            }

            string powerUserUserName = Configuration.GetSection("PowerUserSettings:Login").Value;

            // creating powerUser
            var powerUser = new RaspberryHomeUser
            {
                UserName = powerUserUserName
            };

            string powerUserPassword = Configuration.GetSection("PowerUserSettings:Password").Value;
            var user = await userManager.FindByNameAsync(Configuration.GetSection("PowerUserSettings:Login").Value);

            if (user == null)
            {
                var createPowerUser = await userManager.CreateAsync(powerUser, powerUserPassword);
                if (createPowerUser.Succeeded)
                {
                    await userManager.AddToRoleAsync(powerUser, "Admin");
                }
            }
        }
    }
}