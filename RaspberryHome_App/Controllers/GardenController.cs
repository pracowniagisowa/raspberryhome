using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using RaspberryHome_App.DatabaseModels;
using RaspberryHome_App.DTOs;
using RaspberryHome_App.Services;
using RaspberryHome_App.Services.Garden;
using RaspberryHome_App.Services.Weather;
using RaspberryHome_App.SideServices.Classes;

namespace RaspberryHome_App.Controllers
{
    [ApiController]
    [Route("[controller]/[action]")]
    // [Authorize]
    public class GardenController : ControllerBase
    {
        private readonly IGpioManager _gpioManager;
        private readonly GardenManager _gardenManager;
        private readonly IConfiguration _configuration;
        private readonly ILogger<GardenController> _logger;
        private readonly HomeDbContext _dbContext;
        private readonly TaskManager _taskManager;
        private readonly IServiceScopeFactory _scopeFactory;
        public GardenController(
            HomeDbContext dbContext,
            IGpioManager gpioManager,
            GardenManager gardenManager,
            IConfiguration configuration,
            ILogger<GardenController> logger,
            IServiceScopeFactory scopeFactory)

        {
            _dbContext = dbContext;
            _configuration = configuration;
            _logger = logger;
            _gpioManager = gpioManager;//new GpioManager(gpioController, configuration, _logger);
            _gardenManager = gardenManager; //new GardenManager(_gpioManager, _dbContext, _logger);
            // _taskManager = taskManager;
            _scopeFactory = scopeFactory;
        }

        [HttpGet]
        public ActionResult IsBalconySoilHumid()
        {
            bool isSoilHumid = _gardenManager.IsSoilHumid(
                (int)GpioPinsUsage.SoilHumidityBalconySensorValue,
                (int)GpioPinsUsage.SoilHumidityBalconySensorPower);

            ResponseDTO resp = new ResponseDTO();
            resp.Success = isSoilHumid;
            return Ok(resp);
        }

        [HttpGet]
        public ActionResult IsWindowsillSoilHumid()
        {
            bool isSoilHumid = _gardenManager.IsSoilHumid(
                (int)GpioPinsUsage.SoilHumidityWindowsillSensorValue,
                (int)GpioPinsUsage.SoilHumidityWindowsillSensorPower);

            ResponseDTO resp = new ResponseDTO();
            resp.Success = isSoilHumid;
            return Ok(resp);
        }

        [HttpGet]
        public async Task<ActionResult> WaterBalcony()
        {
            var action = new WaterPlantsAction(_scopeFactory, _logger);
            ActionResponse actionResp = await action.RunAsync();

            return Ok(actionResp);
        }

        [HttpGet]
        public ActionResult GetWateringBalconyHistory()
        {
            var wateringHistory = _gardenManager.GetWateringBalconyHistory(2);
            return Ok(wateringHistory);
        }

        [HttpGet]
        public ActionResult GetSoilHumidityHistory()
        {
            var history = _dbContext.SoilHumidityHistory;

            return Ok(history);
        }

        [HttpGet]
        public ActionResult SetWateringTimeAndIterations(int time, int iterations)
        {
            var settingService = new SettingsService(_dbContext);
            settingService.SetIntValue("WateringBalconyDuration", time);
            settingService.SetIntValue("WateringbalconyIterationsCount", iterations);

            return Ok();
        }

        [HttpGet]
        public async Task<ActionResult> EmailIfToDry()
        {
            var action = new DryHomePlantsNotifireAction(_scopeFactory, _logger);
            await action.RunAsync();

            return Ok();
        }

        [HttpGet]
        public async Task<ActionResult> TomorrowFrost()
        {
            // var weatherService = new OpenWeatherMapService();
            // var tommorowFrostDict = weatherService.TomorrowFrost();

            var action = new TomorrowFrostNotifireAction(_scopeFactory, _logger);
            await action.RunAsync();

            return Ok();
        }
    }
}