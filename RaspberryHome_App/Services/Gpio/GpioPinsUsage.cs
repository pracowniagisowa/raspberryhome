namespace RaspberryHome_App.Services
{
    public enum GpioPinsUsage
    {
        SoilHumidityBalconySensorValue = 27,
        SoilHumidityBalconySensorPower = 22,
        SoilHumidityWindowsillSensorValue = 18,
        SoilHumidityWindowsillSensorPower = 17,
        WaterPumpSwitch = 2,
        TestLED = 21,
        MotionDetector = 26,
        TestSwitch = 20,

    }
}