export class ActionDTO {
  periodActionId: number;
  technicalName: string;
  actionTimes: [];
  actionFrequency: ActionFrequencyDTO;
  isActive: boolean;
  description: string;
}

export class ActionFrequencyDTO {
  actionFrequencyId: number;
  startTime: number;
  frequency: number;
}
