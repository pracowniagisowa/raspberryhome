using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.ApiAuthorization.IdentityServer;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using RaspberryHome_App.DatabaseModels.Garden;
using RaspberryHome_App.DatabaseModels.Identity;
using RaspberryHome_App.Models.Auth;

namespace RaspberryHome_App.DatabaseModels
{
    public class HomeDbContext : IdentityDbContext<RaspberryHomeUser, Role, Guid>
    {
        public HomeDbContext(
            DbContextOptions<HomeDbContext> options)
            : base(options)
        {
        }

        public DbSet<GpioPinState> GpioPinStates { get; set; }
        public DbSet<PeriodAction> PeriodActions { get; set; }
        public DbSet<PeriodActionHistory> PeriodActionHistories { get; set; }
        public DbSet<SoilHumidity> SoilHumidityHistory { get; set; }
        public DbSet<TestEntity> TestEntities { get; set; }
        public DbSet<SettingDb> SettingsDb { get; set; }
        public DbSet<ThermometerFullInfo> ThermometerFullInfos { get; set; }

        // protected override void OnConfiguring (DbContextOptionsBuilder options) => options.UseSqlite (
        //     _configuration.GetConnectionString ("RaspberryHomeServerConnection")
        // );

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<SettingDb>().HasData(
                new SettingDb("WateringBalconyDuration", 60)
                {
                    SettingDbId = 1
                },
                new SettingDb("WateringbalconyIterationsCount", 2)
                {
                    SettingDbId = 2
                }
            );
        }
    }
}