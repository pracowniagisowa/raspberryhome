export const AppUrl = {
  SignIn: "/auth/signIn",
  SignUp: "/auth/signUp",
  GetUser: "/auth/GetUser",
  PostPeriodAction: "/action/PostPeriodAction",
  GetActions: "/action/GetActions",
  SetActionActive: "/action/SetActionActive",
  GetSettings: "/gpio/GetSettings",
  PostSettingsDb: "/gpio/PostSettingsDb",
  SwichPinValue: "/gpio/SwichPinValue",
  SetValueHighAtPin: "/gpio/SetValueHighAtPin",
  SetValuelowAtPin: "/gpio/SetValuelowAtPin",
  GetPinValue: "/gpio/GetPinValue",
  GetPinsFromDb: "/gpio/GetPinsFromDb",
  IsBalconySoilHumid: "/garden/IsBalconySoilHumid",
  IsWindowsillSoilHumid: "/garden/IsWindowsillSoilHumid",
  WaterBalcony: "/garden/WaterBalcony",
  GetWateringBalconyHistory: "/garden/GetWateringBalconyHistory",
  GetSoilHumidityHistory: "/garden/GetSoilHumidityHistory",
  SetWateringTimeAndIterations: "/garden/SetWateringTimeAndIterations",
  GetThermometesFullInfo: "/home/GetThermometesFullInfo",
  PutThermometerNameByThermometerID: "/home/PutThermometerNameByThermometerID",
};

export const AppKey = {
  cookieToken: "raspToken",
};

export const ApplicationName = "Raspberry Home";

export const AppPaths = {
  DefaultLoginRedirectPath: "/",
  UnauthorizedPath: "/login",
  GpioPath: "/gpio",
  HomePath: "/",
  SettingsPath: "/settings",
};
