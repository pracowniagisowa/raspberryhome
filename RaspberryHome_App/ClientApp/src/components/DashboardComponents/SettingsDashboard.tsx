import React from "react";
import ReactDOM from 'react-dom';

import { Theme, createStyles, makeStyles } from '@material-ui/core/styles';
import Accordion from '@material-ui/core/Accordion';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import Typography from '@material-ui/core/Typography';
import ExpandMore from '@material-ui/icons/ExpandMore';
import { Alert, AlertTitle } from '@material-ui/lab';
import { ActionDTO } from "../../classes/dtos/ActionDTO";
import Cookies from "universal-cookie";
import { AppKey, AppUrl } from "../../AppConstans";
import ActionComponent from "./ActionComponent";
import { Button, Checkbox, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle, FormControlLabel, Grid, Switch, TextField, useMediaQuery, useTheme, WithStyles, withStyles, withTheme } from "@material-ui/core";
import { compose } from "recompose";
import ActionPopupEdit from "./ActionPopupEdit";

export interface SettingsDashboardProps extends WithStyles<typeof styles> {

}

export interface SettingsDashboardState {
    actions: ActionDTO[];
    // editDialogOpen: boolean;
    // currentAction: ActionDTO;
    // isCurrentActionEveryday: boolean;
    // frequencyLabel: string;
}

class SettingsDashboard extends React.Component<any, SettingsDashboardState> {

    private actionComponents: JSX.Element[];
    public cookies: Cookies;
    public token: string;
    public dialogRef: any;

    constructor(props: any) {
        super(props);
        this.state = {
            actions: new Array<ActionDTO>(),
            // editDialogOpen: false,
            // currentAction: new ActionDTO(),
            // isCurrentActionEveryday: false,
            // frequencyLabel: "",
        };

        this.cookies = new Cookies();
        this.token = this.cookies.get(AppKey.cookieToken);
        this.dialogRef = React.createRef();

    }

    componentDidMount() {
        this.token = this.cookies.get(AppKey.cookieToken);
        this.updateActions();
    }

    render() {
        const { classes, theme } = this.props;
        this.actionComponents = this.state.actions.map((x, index) =>
            <ActionComponent
                key={index}
                action={x}
                token={this.token}
                onEditAction={this.editActionDialog} />)

        return (

            <React.Fragment>
                <br />
                <div style={{ width: "100%" }}>
                    <Accordion expanded={true}>
                        <AccordionSummary
                            expandIcon={<ExpandMore />}
                            aria-controls="panel1a-content"
                            id="panel1a-header"
                        >
                            <Typography className="AccordionHeading">Akcje</Typography>
                        </AccordionSummary>
                        <AccordionDetails>

                            <Grid container spacing={3}>
                                {this.actionComponents}
                            </Grid>

                        </AccordionDetails>
                    </Accordion>
                    <Accordion>
                        <AccordionSummary
                            expandIcon={<ExpandMore />}
                            aria-controls="panel2a-content"
                            id="panel2a-header"
                        >
                            <Typography className={"AccordionHeading"}>Inne</Typography>
                        </AccordionSummary>
                        <AccordionDetails>
                            <Typography>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse malesuada lacus ex,
                                sit amet blandit leo lobortis eget.
                            </Typography>
                        </AccordionDetails>
                    </Accordion>

                </div>
                <ActionPopupEdit
                    ref={this.dialogRef}
                    onUpdateActions={this.handleUpdateActions} />

            </React.Fragment >
        );
    }

    handleUpdateActions = () => {
        console.log('handleUpdateActions');
        this.updateActions();
    }

    updateActions() {
        fetch(AppUrl.GetActions,
            {
                method: "GET",
                headers: new Headers({
                    Authorization:
                        "Bearer " + this.token
                }),
            })
            .then(resp => resp.json())
            .then(json => {
                this.setState({ actions: json });
                // for (let actionComp of this.actionComponents) {
                //     actionComp.

                // }
            })
    }

    editActionDialog = (action: ActionDTO) => {
        console.log("editActionDialog", action);

        this.dialogRef.current.editActionOpenDialog(action);
    }

    // editActionDialog_onClose = () => {
    //     this.setState({
    //         editDialogOpen: false,
    //     });
    // }

    // actionIsActive_onChange = (e: any) => {
    //     let currentAction = this.state.currentAction;
    //     currentAction.isActive = e.target.checked;
    //     this.setState({
    //         currentAction: currentAction,
    //     });
    // }

    // isCurrentActionEveryday_onChange = (e: any) => {
    //     let isEveryday: boolean = e.target.checked;
    //     let freqLabel: string = "";
    //     if (isEveryday) {
    //         freqLabel = "Częstotliwosć: co 24 h";
    //     } else {
    //         freqLabel = "24 h"
    //     }
    //     this.setState({
    //         isCurrentActionEveryday: isEveryday,
    //         frequencyLabel: freqLabel,
    //     });
    // }

}

// const theme = useTheme();
// const fullScreen = useMediaQuery(theme.breakpoints.down('xs'));


const styles = (theme: any) => createStyles({
    root: {
        width: '100%',
    },
    heading: {
        fontSize: theme.typography.pxToRem(15),
        fontWeight: theme.typography.fontWeightRegular,
    },
    timeField: {
        marginLeft: theme.spacing(1),
        marginRight: theme.spacing(1),
        width: 85,
    },
    checkBoxField: {
        marginLeft: theme.spacing(1),
        marginRight: theme.spacing(1),
        height: 49,
        width: 40,
    },
    checkBoxFieldWide: {
        marginLeft: theme.spacing(1),
        marginRight: theme.spacing(1),
        width: 170,
        height: 49,
    },
    dialog: {
        margin: "0px"
    }

});


export default compose(

    withTheme,
    withStyles(styles))
    (SettingsDashboard);

// export default withTheme(SettingsDashboard);