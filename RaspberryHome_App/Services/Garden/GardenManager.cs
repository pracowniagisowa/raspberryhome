using System;
using System.Collections.Generic;
using System.Device.Gpio;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using RaspberryHome_App.DatabaseModels;
using RaspberryHome_App.DatabaseModels.Identity;

namespace RaspberryHome_App.Services.Garden
{
    public class GardenManager
    {
        private readonly IGpioManager _gpioManager;
        private readonly ILogger _logger;
        private readonly IServiceScopeFactory _scopeFactory;
        private readonly IConfiguration _configuration;
        private UserManager<RaspberryHomeUser> _userManager;

        public GardenManager(
            IGpioManager gpioManager,
            IServiceScopeFactory scopeFactory,
            ILogger<GardenManager> logger
            // EmailService emailService
            // IConfiguration configuration,
            // UserManager<RaspberryHomeUser> userManager
            )
        {
            _scopeFactory = scopeFactory;
            _gpioManager = gpioManager;
            _logger = logger;

            // _configuration = configuration;
            // _userManager = userManager;
        }

        public bool IsSoilHumid(int sensorValuePinId, int sensorPowerPinId)
        {
            _gpioManager.SetPinState(sensorPowerPinId, GpioPinValue.High);
            Thread.Sleep(500);
            GpioPinValue pinValue = _gpioManager.GetPinValue(sensorValuePinId);
            _gpioManager.SetPinState(sensorPowerPinId, GpioPinValue.Low);

            return pinValue == GpioPinValue.Low;
        }

        public void WaterPlants(int waterPumpSwitchPinID)
        {
            _logger.LogInformation($"Inside watering balcony...");

            int wateringDuration = 0;
            int wateringCyclesQuantity = 0;
            using (var scope = _scopeFactory.CreateScope())
            {
                var dbContext = scope.ServiceProvider.GetRequiredService<HomeDbContext>();
                var settingService = new SettingsService(dbContext);
                wateringDuration = settingService.GetIntValue("WateringBalconyDuration");
                wateringCyclesQuantity = settingService.GetIntValue("WateringbalconyIterationsCount");
            }

            for (int i = 0; i < wateringCyclesQuantity; i++)
            {
                _logger.LogInformation($"i = {i}");

                _gpioManager.SetPinState(waterPumpSwitchPinID, GpioPinValue.High);
                Thread.Sleep(TimeSpan.FromSeconds(wateringDuration));
                _gpioManager.SetPinState(waterPumpSwitchPinID, GpioPinValue.Low);
                Thread.Sleep(TimeSpan.FromSeconds(15));
            }
        }

        public List<PeriodActionHistory> GetWateringBalconyHistory(int weeks = 1)
        {
            DateTime dateFrom = DateTime.Now.AddDays(-1 * weeks * 7);
            List<PeriodActionHistory> wateringActions = new List<PeriodActionHistory>();
            using (var scope = _scopeFactory.CreateScope())
            {
                var dbContext = scope.ServiceProvider.GetRequiredService<HomeDbContext>();
                wateringActions = dbContext.PeriodActionHistories
                    .Where(x => x.InitTime >= dateFrom && x.IsActionTriggered == true && x.TechnicalName == "WaterPlantsAction")
                    .OrderByDescending(x => x.InitTime)
                    .ToList();

                return wateringActions;
            }
        }

        
    }
}