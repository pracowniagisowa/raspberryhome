using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using RaspberryHome_App.SideServices;

namespace RaspberryHome_App.DatabaseModels
{
    public class PeriodAction
    {
        [Key]
        public int PeriodActionId { get; set; }
        public string TechnicalName { get; set; }
        public List<ActionTime> ActionTimes { get; set; }
        public ActionFrequency ActionFrequency { get; set; }
        public bool IsActive { get; set; }
        public string Description { get; set; }
        [NotMapped]
        public BaseAction Action { get; set; }


        public PeriodAction()
        {
            ActionTimes = new List<ActionTime>();
        }

        public PeriodAction(string technicalName, string description, BaseAction action)
            : this()
        {
            TechnicalName = technicalName;
            Description = description;
            Action = action;
        }

        public bool CanRunNow(DateTime periodTime, out string message)
        {
            message = String.Empty;
            if (IsActive)
            {
                DateTime now = periodTime;
                int nowTotalSeconds = now.Hour * 60 * 60 + now.Minute * 60 + now.Second;
                var actionTimes = GetActionTimes();
                bool result = false;
                foreach (var actionTime in actionTimes)
                {
                    if (IsCloseEnough(nowTotalSeconds, actionTime))
                    {
                        result = true;
                        break;
                    }
                }
                // var result = actionTimes.Any(x => IsCloseEnough(nowTotalSeconds, x));
                var duration = DateTime.Now - now;
                message = $"CanRunNow duration: {duration}";
                return result;
            }
            return false;

        }

        private bool IsCloseEnough(int timeNowSec, int timeToCompare)
        {
            int timeToCompareSec = timeToCompare * 60;

            return timeNowSec >= timeToCompareSec && timeNowSec < timeToCompareSec + 5;
        }

        private List<int> GetActionTimes()
        {
            List<int> result = new List<int>();
            if (ActionTimes == null || ActionTimes.Count == 0)
            {
                if (ActionFrequency != null)
                {
                    if (ActionFrequency.Frequency == 0)
                    {
                        result.Add(ActionFrequency.StartTime);
                    }
                    else
                    {
                        for (int i = ActionFrequency.StartTime; i < 24 * 60; i += ActionFrequency.Frequency)
                        {
                            result.Add(i);
                        }
                        // poniższa pętla jes zła. 
                        for (int i = ActionFrequency.StartTime - ActionFrequency.Frequency; i > 0; i -= ActionFrequency.Frequency)
                        {
                            result.Add(i);
                        }
                    }
                }
            }
            if (ActionFrequency == null)
            {
                foreach (var time in ActionTimes)
                {
                    if (time.IsActive)
                    {
                        result.Add(time.TimeTotalMinutes);
                    }
                }
            }

            return result;
        }
    }
}