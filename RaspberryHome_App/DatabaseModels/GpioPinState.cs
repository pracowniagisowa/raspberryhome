using System.ComponentModel.DataAnnotations;
using System.Device.Gpio;
using RaspberryHome_App.Services;

namespace RaspberryHome_App.DatabaseModels
{
    public class GpioPinState
    {
        [Key]
        public int GpioStateId { get; set; }

        /// <summary>
        /// Phisical pin number
        /// </summary>
        /// <value></value>
        public int PinPhisicalNo { get; set; }

        /// <summary>
        /// Name pin number
        /// </summary>
        /// <value></value>
        public int PinNo { get; set; }

        public bool IsOpen { get; set; }

        /// <summary>
        /// Pin mode: Input / Output
        /// </summary>
        /// <value></value>
        public GpioPinMode PinMode { get; set; }

        // /// <summary>
        // /// Pin value: High / Low
        // /// </summary>
        // /// <value></value>
        // public PinValue PinValue { get; set; }

        public bool IsStateHigh { get; set; }

        public GpioPinState(int pinNo)
        {
            PinNo = pinNo;
        }

        public string Comment { get; set; }

    }
}