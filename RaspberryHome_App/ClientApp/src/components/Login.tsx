import React from "react";
import {
  Avatar,
  Button,
  Checkbox,
  Container,
  CssBaseline,
  FormControlLabel,
  Grid,
  Link,
  TextField,
  Typography
} from "@material-ui/core";
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import { createStyles, withStyles, WithStyles, withTheme } from "@material-ui/core/styles";
import { withRouter } from "react-router-dom";
import Cookies from "universal-cookie";
import { AppKey, AppPaths, AppUrl } from "../AppConstans";
import { compose } from "recompose";


export interface LoginProps extends WithStyles<typeof styles> { }

export interface LoginState {
  userName: string,
  password: string
}

class Login extends React.Component<any, LoginState> {
  cookies: Cookies;

  constructor(props: any) {
    super(props);
    this.state = {
      userName: "",
      password: ""
    };

    this.cookies = new Cookies();

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleLoginChange = this.handleLoginChange.bind(this);
    this.handlePasswordChange = this.handlePasswordChange.bind(this);
  }

  render() {
    const { classes } = this.props;

    return (
      <React.Fragment>
        <Container component="main" maxWidth="xs">

          <CssBaseline />
          <div className={classes.paper}>
            <Avatar className={classes.avatar}>
              <LockOutlinedIcon />
            </Avatar>
            <Typography component="h1" variant="h5">
              Zaloguj się
          </Typography>
            <form className={classes.form} noValidate>
              <TextField
                variant="outlined"
                margin="normal"
                required
                fullWidth
                id="email"
                label="Email Address"
                name="email"
                autoComplete="email"
                autoFocus
                onChange={this.handleLoginChange.bind(this)}

              />
              <TextField
                variant="outlined"
                margin="normal"
                required
                fullWidth
                name="password"
                label="Password"
                type="password"
                id="password"
                autoComplete="current-password"
                onChange={this.handlePasswordChange.bind(this)}
              />
              <FormControlLabel
                control={<Checkbox value="remember" color="primary" />}
                label="Remember me"
              />
              <Button
                type="submit"
                fullWidth
                variant="contained"
                color="primary"
                className={classes.submit}
                onClick={this.handleSubmit.bind(this)}
              >
                Sign In
          </Button>
              <Grid container>
                <Grid item xs>
                  <Link href="#" variant="body2">
                    Forgot password?
              </Link>
                </Grid>
                <Grid item>
                  <Link href="#" variant="body2">
                    {"Don't have an account? Sign Up"}
                  </Link>
                </Grid>
              </Grid>
            </form>
          </div>
        </Container>

      </React.Fragment>
    );
  }

  handleLoginChange(e: any) {
    console.log("handleLoginChange: ", e.target.value);
    this.setState({
      userName: e.target.value
    })
  }

  handlePasswordChange(e: any) {
    console.log("handlePassChange: ", e.target.value);
    this.setState({
      password: e.target.value
    })
  }

  handleSubmit(event: any) {
    let data = {
      userName: this.state.userName,
      password: this.state.password
    }


    fetch(AppUrl.SignIn, {
      method: "POST",
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(data)
    })
      .then(resp => resp.json())
      .then(json => this.afterSignIn(json))
      .catch(error => console.log(error))




    // this.props.history.push(AppPaths.HomePath)
    event.preventDefault();
  }

  afterSignIn(json: any) {
    console.log("json", json);
    if (json.isAuthorized) {
      this.cookies.set(AppKey.cookieToken, json.token);
      this.props.history.push(AppPaths.HomePath)
    }
  }
}

const styles = (theme: any) => createStyles({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
});

export default compose(
  withRouter,
  withStyles(styles))
  (Login);


