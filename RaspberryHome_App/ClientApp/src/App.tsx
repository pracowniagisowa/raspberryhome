import React from "react";
import logo from ".\\logo.svg";
import "./css/App.css";
import "./css/Accordeon.css";
import "./css/Login.css";
import Layout from "./components/Layout";
import { BrowserRouter } from "react-router-dom";
import { useHistory } from "react-router-dom";

function App() {
  //   const history = useHistory();

  return (
    <BrowserRouter>
      <Layout />
    </BrowserRouter>
  );
}

export default App;
