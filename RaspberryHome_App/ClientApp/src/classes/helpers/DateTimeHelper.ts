export abstract class DateTimeHelper {
  public static totalMinutsToTimeString(
    totalMinutes: number,
    is24Limit: boolean = true
  ): string {
    let hour = Math.floor(totalMinutes / 60);
    let minute = totalMinutes - hour * 60;
    let minuteStr = "";
    if (minute < 10) {
      minuteStr = "0" + minute;
    } else {
      minuteStr = minute.toString();
    }
    // console.log("minuteStr", minuteStr);

    let hourString = "";
    if (is24Limit && hour > 23) {
      hour = hour % 24;
      hourString = hour.toString();
    }

    if (hour < 10) {
      hourString = "0" + hour.toString();
    } else {
      hourString = hour.toString();
    }
    // console.log("hourString", hourString);

    let hourAndMinutes = hourString + ":" + minuteStr;
    return hourAndMinutes;
  }

  public static timeStringToTotalMinutes(timeString: string): number {
    console.log("tsttm timeString", timeString);
    if (timeString) {
      let hourAndMinutes = timeString.split(":");
      if (hourAndMinutes.length == 2) {
        let hour = +hourAndMinutes[0];
        console.log("hour", hour, !isNaN(hour));
        let minute = +hourAndMinutes[1];
        console.log("minute", minute, !isNaN(minute));

        if (!isNaN(hour) && !isNaN(minute) && minute < 60) {
          let totalMinutes = hour * 60 + minute;
          console.log("total minutes", totalMinutes);
          return totalMinutes;
        }
      }
    }
    return 0;
  }
}
