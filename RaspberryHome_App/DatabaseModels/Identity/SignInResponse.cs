namespace RaspberryHome_App.DatabaseModels.Identity
{
    public class SignInResponse
    {
        public bool IsAuthorized { get; set; }
        public bool IsPasswordChangeRequired { get; set; }
        public string Token { get; set; }
        public string Message { get; set; }
    }
}