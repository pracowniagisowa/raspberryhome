using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using RaspberryHome_App.DatabaseModels;
using RaspberryHome_App.DTOs;

namespace RaspberryHome_App.Services.Home
{
    public class HomeService
    {


        public string Path
        {
            get
            {
                string path = String.Empty;
                if (Environment.MachineName == "Bystra")
                {
                    path = @"/usr/w1/w1/devices/";
                }
                else
                {
                    path = @"/sys/bus/w1/devices/";
                }
                return path;
            }
            private set { }
        }

        // string _path = @"/usr/w1/w1/devices/"; //28-0118770ef1ff/w1_slave";
        // string _path = @"/sys/bus/w1/devices/";
        // private readonly IServiceScopeFactory _scopeFactory;
        private readonly HomeDbContext _dbContext;

        public HomeService(HomeDbContext dbContext)
        {
            // _scopeFactory = scopeFactory;
            _dbContext = dbContext;
        }


        public async Task<List<ThermometerFullInfoDTO>> GetThermometerFullInfos()
        {
            var thermometerIDs = GetThermometers();
            List<ThermometerFullInfoDTO> thermDTOs = new List<ThermometerFullInfoDTO>();

            foreach (var thermID in thermometerIDs)
            {
                var thermFullInfo = _dbContext.ThermometerFullInfos.FirstOrDefault(x => x.ThermometerID == thermID);
                if (thermFullInfo == null)
                {
                    continue;
                }
                ThermometerFullInfoDTO thermDTO = new ThermometerFullInfoDTO();
                thermDTO.ThermometerID = thermID;
                thermDTO.Name = thermFullInfo.Name;
                thermDTO.Temperature = await GetTemperature(thermID);

                thermDTOs.Add(thermDTO);
            }

            return thermDTOs;
        }

        public async Task<decimal> GetTemperature(string thermometerID)
        {
            decimal temperature = 0;

            var thermPath = $"{Path}/{thermometerID}";
            DirectoryInfo thermDir = new DirectoryInfo(thermPath);
            var files = thermDir.GetFiles();
            var w1Slave = files.FirstOrDefault(x => x.Name == "w1_slave");
            if (w1Slave != null)
            {
                using (var sr = new StreamReader(w1Slave.FullName))
                {
                    bool found = false;
                    while (!found)
                    {
                        var line = await sr.ReadLineAsync();
                        if (line != null && line.Contains("t="))
                        {
                            var contentSpl = line.Split(" ");
                            var temperatureString = contentSpl.FirstOrDefault(x => x.StartsWith("t="));
                            if (temperatureString != null)
                            {
                                temperatureString = temperatureString.Replace("t=", "");
                                if (decimal.TryParse(temperatureString, out temperature))
                                {
                                }
                            }
                            found = true;
                        }
                    }
                }
            }
            temperature = temperature / 1000;
            temperature = Math.Round(temperature, 1);
            return temperature;
        }

        public List<string> GetThermometers()
        {
            List<string> thermometers = new List<string>();


            if (Directory.Exists(Path))
            {
                DirectoryInfo dirInfo = new DirectoryInfo(Path);
                Debug.WriteLine("Dir name", dirInfo.Name);
                var dirs = dirInfo.GetDirectories();
                foreach (var dir in dirs)
                {
                    if (dir.Name.StartsWith("28"))
                    {
                        thermometers.Add(dir.Name);
                    }
                }
            }
            return thermometers;
        }

        public void UpdateThermometersListInDatabase()
        {
            var currentThermIDs = GetThermometers();
            foreach (var thermID in currentThermIDs)
            {
                if (!_dbContext.ThermometerFullInfos.Any(x => x.ThermometerID == thermID))
                {
                    _dbContext.ThermometerFullInfos.Add(new ThermometerFullInfo(thermID));
                }
            }
            _dbContext.SaveChanges();

        }

    }
}