using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using NLog;
using NLog.Targets;

namespace RaspberryHome_App.Controllers
{
    public class AdminController : ControllerBase
    {
        private readonly ILogger<AdminController> _logger;

        public AdminController(ILogger<AdminController> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        public async Task<ActionResult> GetLogFile(string namePart)
        {
            var test = (FileTarget)LogManager.Configuration.AllTargets.FirstOrDefault(x => x.Name.Contains(namePart));
            if (test == null)
            {
                return NotFound();
            }

            var logEventInfo = new LogEventInfo();
            var filepath = test.FileName.Render(logEventInfo);

            var memory = new MemoryStream();
            using (var stream = new FileStream(filepath, FileMode.Open))
            {
                await stream.CopyToAsync(memory);
            }
            memory.Position = 0;
            return File(memory, "text/plain", Path.GetFileName(filepath));
        }
    }
}