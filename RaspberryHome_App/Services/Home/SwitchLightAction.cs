using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using RaspberryHome_App.SideServices;
using RaspberryHome_App.SideServices.Classes;

namespace RaspberryHome_App.Services.Home
{
    public class SwitchLightAction : BaseAction
    {
        public SwitchLightAction(IServiceScopeFactory scopeFactory, ILogger logger)
            : base(scopeFactory, logger)
        {
            // _gardenManager = gardenManager;
        }

        public override ActionResponse Action()
        {
            ActionResponse response = new ActionResponse();
            using (var scope = _scopeFactory.CreateScope())
            {
                var gpioManager = scope.ServiceProvider.GetRequiredService<IGpioManager>();
                gpioManager.SwichPinValue((int)GpioPinsUsage.TestLED, out string message);
                _logger.LogInformation(message);
            }

            return response;
        }
    }
}