using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using RaspberryHome_App.DatabaseModels;
using RaspberryHome_App.Services;
using RaspberryHome_App.SideServices.Classes;

namespace RaspberryHome_App.SideServices
{
    public abstract class BaseAction
    {
        protected readonly IConfiguration _configuration;
        protected IGpioManager _gpioManager;
        protected readonly String _actionName;
        protected ILogger _logger;
        protected TimeSpan _runDuration;
        protected bool _isRunSucceeded = false;
        protected IServiceScopeFactory _scopeFactory;
        // protected HomeDbContext _dbContext;

        public BaseAction()
        {
        }

        public BaseAction(IServiceScopeFactory scopeFactory, ILogger logger)
        {
            // _dbContext = dbContext;
            _scopeFactory = scopeFactory;
            // _gpioManager = gpioManager;
            _logger = logger;

            ServiceStarter();
        }

        public void LoadProperties(IServiceScopeFactory scopeFactory, ILogger logger)
        {
            // _dbContext = dbContext;
            _scopeFactory = scopeFactory;
            // _gpioManager = gpioManager;
            _logger = logger;

            ServiceStarter();
        }

        private void ServiceStarter()
        {
            using (var scope = _scopeFactory.CreateScope())
            {
                _gpioManager = scope.ServiceProvider.GetRequiredService<IGpioManager>();
            }
        }

        public async Task<ActionResponse> RunAsync()
        {
            return await Task.FromResult<ActionResponse>(ActionStarter());
        }

        public void RunInBackground()
        {
            Task.Run(() => { ActionStarter(); });
        }

        private ActionResponse ActionStarter()
        {
            _logger.LogInformation($"Running action: {this.GetType().Name}");

            var startTime = DateTime.Now;
            string message = string.Empty;
            var response = Action();

            SaveToHistory(response, startTime);

            return response;
        }

        public virtual ActionResponse Action()
        {
            return null;
        } //out bool isTriggering, out bool isSuccesfull, out string message

        private void SaveToHistory(ActionResponse response, DateTime startTime)
        {
            PeriodActionHistory history = new PeriodActionHistory();
            history.TechnicalName = this.GetType().Name;
            history.IsActionTriggered = response.Triggered;
            history.IsCompleted = response.Success;
            history.InitTime = startTime;
            history.EndTime = DateTime.Now;
            history.Comment = response.Message;

            using (var scope = _scopeFactory.CreateScope())
            {
                var dbContext = scope.ServiceProvider.GetRequiredService<HomeDbContext>();
                dbContext.PeriodActionHistories.Add(history);
                dbContext.SaveChanges();
            }

        }
    }
}