using System;
using Microsoft.AspNetCore.Identity;

namespace RaspberryHome_App.Models.Auth
{
    public class Role : IdentityRole<Guid>
    {
        public Role()
        {            
        }
        public Role(string roleName)
            :base(roleName)
            
        {
            
        }
    }
}