using System.ComponentModel.DataAnnotations;

namespace RaspberryHome_App.DatabaseModels
{
    public class ThermometerFullInfo
    {
        [Key]
        public int ThermometerFullInfoID { get; set; }
        public string ThermometerID { get; set; }
        public string Name { get; set; }

        public ThermometerFullInfo(string thermometerID)
        {
            ThermometerID = thermometerID;
        }
    }
}