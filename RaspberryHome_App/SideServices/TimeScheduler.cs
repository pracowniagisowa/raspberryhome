using System;
using System.Collections.Generic;
using System.Device.Gpio;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using RaspberryHome_App.DatabaseModels;
using RaspberryHome_App.Services;
using RaspberryHome_App.Services.Garden;

namespace RaspberryHome_App.SideServices
{
    public class TimeScheduler
    {
        // private GpioController _gpio;
        // private IGpioManager _gpioManager;
        // private GardenManager _gardenManager;
        private ILogger _logger;
        private readonly IServiceScopeFactory _scopeFactory;
        // private TaskManager _taskManager;

        public TimeScheduler(
                            // IGpioManager gpioManager,
                            // GardenManager gardenManager,
                            IServiceScopeFactory scopeFactory,
                            ILogger<TimeScheduler> logger)
        {
            // _gpio = gpioController;
            // _configuration = configuration;
            _scopeFactory = scopeFactory;
            _logger = logger;
            // _gpioManager = gpioManager; //new GpioManager(gpioController, configuration, logger);
        }

        public async Task PeriodActions()
        {
            bool anyActionStarted = false;
            DateTime start = DateTime.Now;
            List<PeriodAction> periodActions = null;
            DateTime periodStart = DateTime.Now;

            using (var scope = _scopeFactory.CreateScope())
            {
                var dbContext = scope.ServiceProvider.GetRequiredService<HomeDbContext>();
                // _logger.LogInformation($"periodStart: {periodStart}");
                periodActions = dbContext.PeriodActions
                                    .Include(x => x.ActionFrequency)
                                    .Include(x => x.ActionTimes)
                                    .ToList();
            }

            foreach (var periodAction in periodActions)
            {
                if (periodAction.CanRunNow(periodStart, out string message))
                {
                    // _logger.LogInformation($"PeriodAction: {periodAction.TechnicalName}");
                    BaseAction action = null;

                    var assembly = Assembly.GetExecutingAssembly();
                    var type = assembly.GetTypes()
                    .First(t => t.Name == periodAction.TechnicalName);

                    action = Activator.CreateInstance(type) as BaseAction;
                    action.LoadProperties(_scopeFactory, _logger);

                    if (action != null)
                    {
                        action.RunInBackground();
                    }
                    else
                    {
                        _logger.LogError("Action not found (is null!!)");
                    }

                    anyActionStarted = true;
                }
            }

            if (anyActionStarted)
            {
                _logger.LogInformation($"Period action duration: {DateTime.Now - start}");
            }
        }
    }
}