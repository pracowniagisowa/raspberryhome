﻿using System;
using System.Device.Gpio;
using System.Threading;
using Unosquare.RaspberryIO;
using Unosquare.RaspberryIO.Abstractions;

namespace RPI_GPIO_Tests
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello GPIO!");
            Console.WriteLine(DateTime.Now.ToString());

            Console.WriteLine("Czy testować piny (y/n)");
            bool goWithPins = "y" == Console.ReadLine();

            if (goWithPins)
            {
                Console.WriteLine("TRUE");

                var pi = Pi.Info.ToString();

                // Console.WriteLine("Podaj nr pinu");
                int pinID = 23;
                Console.WriteLine($"Pin nr {pinID}");
                // string pinIDString = Console.ReadLine();
                // Int32.TryParse(pinIDString, out pinID);



                IGpioHelper gpioHelper = new PiGpioHepler();
                gpioHelper.SetPinState(pinID, 1);
                System.Console.WriteLine("Czekam......");
                Thread.Sleep(2000);
                gpioHelper.SetPinState(pinID, 0);

                Console.WriteLine("A teraz czytanie pinu:");
                // Console.WriteLine("Podaj nr pinu");
                // pinIDString = Console.ReadLine();
                // Int32.TryParse(pinIDString, out pinID);
                var pinReadID = 3;
                Console.WriteLine($"Pin nr {pinReadID}");
                var pinR = Pi.Gpio[pinReadID];
                pinR.PinMode = GpioPinDriveMode.Input;
                var pinValue = pinR.Read();

                Console.WriteLine($"Pin {pinReadID} value is: {pinValue}");
            }

            while (true)
            {
                Console.WriteLine(DateTime.Now.ToString());
                Thread.Sleep(15000);
            }

        }
    }
}