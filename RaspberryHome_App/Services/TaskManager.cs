using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RaspberryHome_App.Services
{
    public class TaskManager
    {
        private Dictionary<Guid, Task> _dict = new Dictionary<Guid, Task>();

        public TaskManager()
        {
        }

        public Guid AddTask(Task task)
        {
            var id = Guid.NewGuid();
            _dict.Add(id, task);
            return id;
        }

        public Guid AddTask(Guid id, Task task)
        {
            _dict.Add(id, task);
            return id;
        }

        public Task GetTask(Guid id)
        {
            Task t = null;
            _dict.TryGetValue(id, out t);
            return t;
        }
    }
}