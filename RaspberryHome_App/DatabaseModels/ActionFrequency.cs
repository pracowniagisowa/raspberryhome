using System.ComponentModel.DataAnnotations;

namespace RaspberryHome_App.DatabaseModels
{
    public class ActionFrequency
    {
        [Key]
        public int ActionFrequencyId { get; set; }
        public int StartTime { get; set; }
        public int Frequency { get; set; }
    }
}