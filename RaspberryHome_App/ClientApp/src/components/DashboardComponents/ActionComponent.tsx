import { Button, Checkbox, createStyles, FormControlLabel, Grid, IconButton, Paper, Switch, TextField, Typography, WithStyles, withStyles } from "@material-ui/core";
import { Edit } from "@material-ui/icons";
import React from "react";
import { AppUrl } from "../../AppConstans";
import { ActionDTO } from "../../classes/dtos/ActionDTO";
import { DateTimeHelper } from "../../classes/helpers/DateTimeHelper";
import { IToken } from "../../classes/IToken";

export interface ActionComponentProps extends WithStyles<typeof styles> {
    action: ActionDTO;
    token: string;
    onEditAction: any;
}

export interface ActionComponentState {
    isActive: boolean;
    startTimeString: string;
    frequencyString: string;
}

class ActionComponent extends React.Component<ActionComponentProps, ActionComponentState> {
    constructor(props: ActionComponentProps) {
        super(props);
        this.state = {
            isActive: this.props.action.isActive,
            startTimeString: DateTimeHelper.totalMinutsToTimeString(this.props.action.actionFrequency.startTime),
            frequencyString: DateTimeHelper.totalMinutsToTimeString(this.props.action.actionFrequency.frequency)
        };
    }
    render() {
        const { classes } = this.props;

        return (
            <React.Fragment>
                <Paper className={classes.paper}>
                    <Grid
                        container
                        spacing={1}
                    >
                        <Grid item xs={12} sm={12} md={5} lg={4}>
                            <Typography variant="button">
                                {this.props.action.description}
                            </Typography>
                        </Grid>
                        <Grid item xs={2} sm={2} md={1} lg={1}>
                            <Switch
                                checked={this.state.isActive}
                                onChange={this.isActive_onChange.bind(this)}
                                color="primary"
                                size="small"
                                inputProps={{ 'aria-label': 'primary checkbox' }}
                            />
                        </Grid>
                        <Grid item xs={4} sm={3} md={2} lg={1}>
                            <TextField
                                id="time"
                                value={this.state.startTimeString}
                                label="Godz. rozp."
                                className={classes.timeField}
                                type="time"
                                disabled={true}
                                InputLabelProps={{
                                    shrink: true,
                                }}
                                inputProps={{
                                    step: 300, // 5 min
                                }}
                            />
                        </Grid>
                        <Grid item xs={4} sm={3} md={2} lg={1}>
                            <TextField
                                id="time"
                                value={this.state.frequencyString}
                                label="Częstotliwość"
                                className={classes.timeField}
                                type="time"
                                disabled={true}
                                InputLabelProps={{
                                    shrink: true,
                                }}
                                inputProps={{
                                    step: 300, // 5 min
                                }}
                            />
                        </Grid>
                        <Grid item xs={2} sm={2} md={1} lg={1}>

                            <IconButton
                                color="primary"
                                onClick={() => this.props.onEditAction(this.props.action)}>
                                <Edit />
                            </IconButton>
                        </Grid>

                    </Grid>
                </Paper>

                <br />
            </React.Fragment>
        );
    }

    isActive_onChange = (e: any) => {

        var checked = e.target.checked;
        console.log("isActive e", checked);
        this.setState({
            isActive: checked,
        });
        fetch(AppUrl.SetActionActive + "?actionID=" + this.props.action.periodActionId + "&isActive=" + checked, {
            method: "PUT",
            headers: new Headers({
                Authorization:
                    "Bearer " + this.props.token
            }),
        })
    }

    rerender() {
        this.forceUpdate();
    }
}


const styles = (theme: any) => createStyles({
    timeField: {
        marginLeft: theme.spacing(1),
        marginRight: theme.spacing(1),
        width: 85,
    },
    paper: {
        padding: theme.spacing(1),
        width: "100%",
        textAlign: 'left',
        color: theme.palette.text.secondary,
    }
});

export default withStyles(styles)(ActionComponent);