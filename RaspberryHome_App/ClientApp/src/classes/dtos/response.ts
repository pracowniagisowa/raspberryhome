export class ResponseDTO {
    public id: string;
    public success: boolean;
    public message: string;
}