import React from "react";
// import Card from "react-bootstrap/Card";
import Button from "react-bootstrap/Button";
import Modal from "react-bootstrap/Modal";
import Table from "react-bootstrap/Table";
import { ResponseDTO } from "../../classes/dtos/response";
import { AppKey, AppUrl } from "../../AppConstans";
import Cookies from "universal-cookie";

import { Theme, createStyles, makeStyles } from '@material-ui/core/styles';
import Accordion from '@material-ui/core/Accordion';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import Typography from '@material-ui/core/Typography';
import ExpandMore from '@material-ui/icons/ExpandMore';
import { Alert, AlertTitle } from "@material-ui/lab";
import { Card, Grid, IconButton, Tab, TableBody, TableCell, TableHead, TableRow, TextField } from "@material-ui/core";
import { Edit } from "@material-ui/icons";
import ThermometerFullInfo from "./ThermometerFullInfo";

export interface HomeDashboardProps { }

export interface HomeDashboardState {
	isBalconySoilHumid: boolean;
	isWindowsillSoilHumid: boolean;
	isWateringHistoryModalShow: boolean;
	wateringHistory: [];
	thermometersFullInfo: [],
}

class HomeDashboard extends React.Component<HomeDashboardProps, HomeDashboardState> {

	public cookies: Cookies;
	public token: string;

	constructor(props: HomeDashboardProps) {
		super(props);
		this.state = {
			isBalconySoilHumid: false,
			isWindowsillSoilHumid: false,
			wateringHistory: [],
			isWateringHistoryModalShow: false,
			thermometersFullInfo: [],
		};

		this.cookies = new Cookies();
		this.token = this.cookies.get(AppKey.cookieToken);

		this.getWateringHistory = this.getWateringHistory.bind(this);

		this.handleClose = this.handleClose.bind(this);
		this.handleShow = this.handleShow.bind(this);
	}

	componentDidMount() {
		this.token = this.cookies.get(AppKey.cookieToken);
		this.updateBalconySoilHumidity();
		this.updateWindowsillSoilHumidity();
		this.updateThermometersFullInfo();
		// this.getWateringHistory();
	}

	handleClose() {
		this.setState({ isWateringHistoryModalShow: false });
	}

	handleShow() {
		this.setState({ isWateringHistoryModalShow: true });
	}

	render() {
		return (
			<div>
				<br />
				<Accordion expanded={true}>
					<AccordionSummary
						expandIcon={<ExpandMore />}
						aria-controls="panel0a-content"
						id="panel1a-header"
					>
						<Typography className="AccordionHeading">Termometry</Typography>
					</AccordionSummary>
					<AccordionDetails>
						<Grid container
							direction="column"
							spacing={2}>
							{this.state.thermometersFullInfo.map((row: any, idx: number) => (
								<ThermometerFullInfo key={idx} idx={idx + 1} thermometerFullInfo={row} token={this.token} />
							))}

						</Grid>
					</AccordionDetails>
				</Accordion>
				<Accordion>
					<AccordionSummary
						expandIcon={<ExpandMore />}
						aria-controls="panel1a-content"
						id="panel1a-header"
					>
						<Typography className="AccordionHeading">Podlewanie</Typography>
					</AccordionSummary>
					<AccordionDetails>
						<Grid container
							direction="column"
							spacing={2}>
							<SoilInfo locationName="Balkon" isHumid={this.state.isBalconySoilHumid} />
							<SoilInfo locationName="Parapet" isHumid={this.state.isWindowsillSoilHumid} />
						</Grid>
					</AccordionDetails>
				</Accordion>
				<Accordion>
					<AccordionSummary
						expandIcon={<ExpandMore />}
						aria-controls="panel2a-content"
						id="panel2a-header"
					>
						<Typography className={"AccordionHeading"}>Oświetlenie</Typography>
					</AccordionSummary>
					<AccordionDetails>
						<Typography>
							Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse malesuada lacus ex,
							sit amet blandit leo lobortis eget.
                            </Typography>
					</AccordionDetails>
				</Accordion>
				<Accordion>
					<AccordionSummary
						expandIcon={<ExpandMore />}
						aria-controls="panel3a-content"
						id="panel3a-header"
					>
						<Typography className={"AccordionHeading"}>Inne</Typography>
					</AccordionSummary>
				</Accordion>
			</div>
		);
	}

	displaySoilHumidMessage() {
		let message: string = "Rośliny na balkonie mają ";
		if (this.state.isBalconySoilHumid) {
			message += "wilgotno";
		} else {
			message += "sucho";
		}
		return message;
	}

	updateThermometersFullInfo(): void {
		fetch(AppUrl.GetThermometesFullInfo,
			{
				method: "GET",
				headers: new Headers({
					Authorization:
						"Bearer " + this.token
				}),
			})
			.then(resp => resp.json())
			.then(json => this.setState({ thermometersFullInfo: json }))
			.catch((err) => alert(err));
	}

	updateBalconySoilHumidity(): void {
		fetch(AppUrl.IsBalconySoilHumid,
			{
				method: "GET",
				headers: new Headers({
					Authorization:
						"Bearer " + this.token
				}),
			})
			.then((res) => res.json())
			.then((json) => {
				var responseDTO = json as ResponseDTO;
				this.setState({
					isBalconySoilHumid: responseDTO.success,
				})
			})
			.catch((err) => alert(err));
	}

	updateWindowsillSoilHumidity(): void {
		fetch(AppUrl.IsWindowsillSoilHumid,
			{
				method: "GET",
				headers: new Headers({
					Authorization:
						"Bearer " + this.token
				}),
			})
			.then((res) => res.json())
			.then((json) => {
				var responseDTO = json as ResponseDTO;
				this.setState({
					isWindowsillSoilHumid: responseDTO.success,
				})
			})
			.catch((err) => alert(err));
	}

	onUpdateSoilHumidity() {

	}

	getWateringHistory() {
		console.log("getWateringHistory...");
		fetch(AppUrl.GetWateringBalconyHistory) //"garden/GetWateringBalconyHistory"
			.then((res) => res.json())
			.then((json) => this.setState({ wateringHistory: json }))
			.catch((err) => alert(err));
	}

	thermometerName_onChange = (e: any) => {
		console.log("thermometerName_onChange", e.target.value);
	}

	wateringHistoryModal() {
		// const handleClose = () => setShow(false);
		// const handleShow = () => setShow(true);
		return (
			<React.Fragment>
				<Button variant="primary" onClick={this.handleShow}>
					Historia podlewania
				</Button>

				<Modal
					show={this.state.isWateringHistoryModalShow}
					onHide={this.handleClose}
					backdrop="static"
					keyboard={false}
				>
					<Modal.Header closeButton>
						<Modal.Title>Historia podlewania</Modal.Title>
					</Modal.Header>
					<Modal.Body>
						<Table striped bordered hover size="sm">
							<thead>
								<tr>
									<th>#</th>
									<th>Czas rozpoczęcia</th>
									<th>Komentarz</th>
								</tr>
							</thead>
							<tbody>
								{this.state.wateringHistory.map(
									(action: any, index: number) => {
										return (
											<tr key={index}>
												<td>{index}</td>
												<td>{action.initTime}</td>
												<td>{action.comment}</td>
											</tr>
										);
									}
								)}
							</tbody>
						</Table>
					</Modal.Body>
					<Modal.Footer>
						<Button variant="secondary" onClick={this.handleClose}>
							Close
						</Button>
					</Modal.Footer>
				</Modal>
			</React.Fragment>
		);
	}
}

export interface SoilInfoProps {
	locationName: string;
	isHumid: boolean;
}

export interface SoilInfoState {
}

class SoilInfo extends React.Component<SoilInfoProps, SoilInfoState> {
	constructor(props: SoilInfoProps) {
		super(props);
		this.state = {};
	}
	render() {
		return (
			<React.Fragment>
				{this.props.isHumid &&
					<Alert severity="success">
						<AlertTitle>{this.props.locationName}</AlertTitle>
                        Rośliny podlane
                    </Alert>
				}
				{!this.props.isHumid &&
					<Alert severity="warning">
						<AlertTitle>{this.props.locationName}</AlertTitle>
                        Za sucho
                    </Alert>
				}
			</React.Fragment>
		);
	}
}

// export interface ThermometerFullInfoProps {
// 	thermometerFullInfo: any,
// 	idx: number,
// }

// export interface ThermometerFullInfoState {
// 	name: string,
// 	isEditable: boolean,
// }

// class ThermometerFullInfo extends React.Component<ThermometerFullInfoProps, ThermometerFullInfoState> {
// 	constructor(props: ThermometerFullInfoProps) {
// 		super(props);
// 		this.state = {
// 			name: this.props.thermometerFullInfo.name,
// 			isEditable: false,
// 		};
// 	}
// 	render() {
// 		return (
// 			<React.Fragment>
// 				<Card variant="outlined">
// 					<Grid
// 						container
// 						spacing={1}>
// 						<Grid item xs={1}>
// 							<Typography variant="inherit">
// 								{this.props.idx}
// 							</Typography>
// 						</Grid>
// 						<Grid item xs={6}>
// 							<TextField
// 								id="name"
// 								value={this.state.name}
// 								disabled={!this.state.isEditable}
// 								onChange={this.name_onChange.bind(this)} />
// 						</Grid>
// 						<Grid
// 							item xs={2}
// 						>
// 							<TextField
// 								type="number"
// 								value={this.props.thermometerFullInfo.temperature}
// 							/>
// 						</Grid>
// 						<Grid
// 							item xs={2}>
// 							<IconButton
// 							>
// 								<Edit />
// 							</IconButton>
// 						</Grid>

// 					</Grid>

// 				</Card>
// 			</React.Fragment>
// 		);
// 	}

// 	name_onChange = (e: any) => {
// 		var newName = e.target.value;
// 		console.log(newName);
// 		this.setState({
// 			name: newName,
// 		})
// 	}

// 	thermometerFullInfo_onEditAction = (thermID: string) => {

// 	}
// }

// export default SoilInfo;

export default HomeDashboard;
