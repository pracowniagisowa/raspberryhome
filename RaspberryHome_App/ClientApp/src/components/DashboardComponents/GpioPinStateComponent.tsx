import { Button, Chip, Grid, Hidden, Typography } from "@material-ui/core";
import React from "react";
// import { Button } from "react-bootstrap";
import { IToken } from "../../classes/IToken";
import { PinFromDB } from "../../classes/PinFromDB";
import { InputLabel } from '@material-ui/core';
import { Refresh } from "@material-ui/icons";


export interface GpioPinStateComponentProps extends IToken {
    pinFromDB: PinFromDB;
    onPinsFromDbRefresh: any;
}

export interface GpioPinStateComponentState {

}

class GpioPinStateComponent extends React.Component<GpioPinStateComponentProps, GpioPinStateComponentState> {
    constructor(props: GpioPinStateComponentProps) {
        super(props);
        // this.state = { :  };
    }
    render() {
        return (
            <React.Fragment>
                <Grid
                    container
                    spacing={3}
                    direction="row"
                    justify="flex-start"
                    alignItems="center">
                    <Grid item xs={3} sm={2} md={1}>
                        <Typography variant="button">
                            Pin {this.props.pinFromDB.pinNo}
                        </Typography>

                    </Grid>
                    <Grid item xs={4} sm={3} md={2}>
                        {this.props.pinFromDB.isStateHigh &&
                            <Chip
                                label="Stan wysoki"
                                color="primary" />}
                        {!this.props.pinFromDB.isStateHigh &&
                            <Chip
                                label="Stan niski"
                                color="default" />}

                    </Grid>
                    <Grid item xs={5} sm={4} md={3}>
                        <Button
                            variant="contained"
                            color="primary"
                            size="small"
                            startIcon={<Refresh />}
                            onClick={this.switchPinState_onClick.bind(this)}>
                            <Hidden xsDown>Przełącz stan</Hidden>
                        </Button>
                    </Grid>
                </Grid>
            </React.Fragment>
        );
    }

    switchPinState_onClick = (e: any) => {
        fetch("gpio/SwichPinValue?pinID=" + this.props.pinFromDB.pinNo)
            .then((res) => res.json())

            .then(() => this.props.onPinsFromDbRefresh())
            .catch((err) => alert(err));
    }
}

export default GpioPinStateComponent;