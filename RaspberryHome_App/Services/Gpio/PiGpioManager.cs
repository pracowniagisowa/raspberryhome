using System;
using System.Collections.Generic;
using System.Device.Gpio;
using System.Linq;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using RaspberryHome_App.DatabaseModels;
using Unosquare.RaspberryIO;
using Unosquare.RaspberryIO.Abstractions;
using Unosquare.WiringPi;

namespace RaspberryHome_App.Services
{
    public class PiGpioManager : IGpioManager
    {
        private readonly ILogger<PiGpioManager> _logger;
        private readonly IServiceScopeFactory _scopeFactory;

        public PiGpioManager(IServiceScopeFactory scopeFactory, ILogger<PiGpioManager> logger)
        {
            _logger = logger;
            _scopeFactory = scopeFactory;

            // https://github.com/unosquare/raspberryio#usage
            Pi.Init<BootstrapWiringPi>();
        }

        public GpioPinValue GetPinValue(int pinId, bool saveToDb = true)
        {
            IGpioPin pin = Pi.Gpio[pinId];
            pin.PinMode = GpioPinDriveMode.Input;

            var pinValue = pin.Read();
            if (pinValue)
            {
                return GpioPinValue.High;
            }
            else
            {
                return GpioPinValue.Low;
            }
        }

        public void InitGpio()
        {
            _logger.LogInformation("GPIO initialising");
            List<GpioPinState> pinStates = new List<GpioPinState>();
            using (var scope = _scopeFactory.CreateScope())
            {
                var dbContext = scope.ServiceProvider.GetRequiredService<HomeDbContext>();
                pinStates = dbContext.GpioPinStates.ToList();
            }
            foreach (var pin in pinStates)
            {
                if (pin.IsStateHigh)
                {
                    SetPinState(pin.PinNo, GpioPinValue.High, false);
                }
                else
                {
                    SetPinState(pin.PinNo, GpioPinValue.Low, false);
                }
            }
            _logger.LogInformation("GPIO initialised");
        }

        public bool IsPinStateHighDb(int pinID)
        {
            using (var scope = _scopeFactory.CreateScope())
            {
                var dbContext = scope.ServiceProvider.GetRequiredService<HomeDbContext>();
                var pinState = dbContext.GpioPinStates.FirstOrDefault(x => x.PinNo == pinID);
                if (pinState == null)
                {
                    return false;
                }
                return pinState.IsStateHigh;
            }
        }

        public void SetPinState(int pinId, GpioPinValue pinValue, bool saveToDb = true)
        {
            // _logger.LogInformation($"Seting pin {pinId} state");
            var pin = Pi.Gpio[pinId];
            pin.PinMode = GpioPinDriveMode.Output;

            // zastanowić się co zrobić jak jest otwarty, ale na input
            pin.Write(pinValue == GpioPinValue.High);
            _logger.LogInformation($"Pin {pinId} state set to {pinValue.ToString()}");
            if (saveToDb)
            {
                SavePinState(pinId, GpioPinMode.Output, pinValue);
            }
        }

        public bool SwichPinValue(int pinId, out string message)
        {
            message = String.Empty;
            try
            {
                if (IsPinStateHighDb(pinId))
                {
                    SetPinState(pinId, GpioPinValue.Low);
                    message += PinValue.Low.ToString();
                }
                else
                {
                    SetPinState(pinId, GpioPinValue.High);
                    message += PinValue.High.ToString();
                }
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
            return true;
        }

        private void SavePinState(int pinId, GpioPinMode pinMode, GpioPinValue pinValue)
        {
            using (var scope = _scopeFactory.CreateScope())
            {
                var dbContext = scope.ServiceProvider.GetRequiredService<HomeDbContext>();
                var pinState = dbContext.GpioPinStates.FirstOrDefault(x => x.PinNo == pinId);
                if (pinState == null)
                {
                    pinState = new GpioPinState(pinId);
                    pinState.IsOpen = true;
                    pinState.PinMode = pinMode;
                    pinState.IsStateHigh = pinValue == GpioPinValue.High;
                    dbContext.GpioPinStates.Add(pinState);

                }
                else
                {
                    pinState.IsOpen = true;
                    pinState.PinMode = pinMode;
                    pinState.IsStateHigh = pinValue == GpioPinValue.High;
                }
                dbContext.SaveChanges();
            }
        }
    }
}