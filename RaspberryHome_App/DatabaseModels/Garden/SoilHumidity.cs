using System;
using System.ComponentModel.DataAnnotations;

namespace RaspberryHome_App.DatabaseModels.Garden
{
    public class SoilHumidity
    {
        [Key]
        public int SoilHumidityId { get; set; }
        public bool IsHumid { get; set; }
        public DateTime DateTime { get; set; }

        public SoilHumidity()
        {
            DateTime = DateTime.Now;
        }
    }
}