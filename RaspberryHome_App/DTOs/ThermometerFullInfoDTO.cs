namespace RaspberryHome_App.DTOs
{
    public class ThermometerFullInfoDTO
    {
        public int ThermometerFullInfoID { get; set; }
        public string ThermometerID { get; set; }
        public string Name { get; set; }
        public decimal Temperature { get; set; }
    }
}