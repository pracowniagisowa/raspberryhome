import { createStyles } from "@material-ui/core";
import React from "react";
import { Button, Checkbox, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle, FormControlLabel, Grid, Switch, TextField, useMediaQuery, useTheme, WithStyles, withStyles, withTheme } from "@material-ui/core";
import { ActionDTO } from "../../classes/dtos/ActionDTO";
import { DateTimeHelper } from "../../classes/helpers/DateTimeHelper";
import { AppUrl } from "../../AppConstans";


export interface ActionPopupEditProps extends WithStyles<typeof styles> {
    ref: any;
    onUpdateActions: any;
}

export interface ActionPopupEditState {
    currentAction: ActionDTO;
    isCurrentActionEveryday: boolean;
    frequencyLabel: string;
    editDialogOpen: boolean;
    startTimeString: string;
    frequencyString: string;
}

class ActionPopupEdit extends React.Component<ActionPopupEditProps, ActionPopupEditState> {
    constructor(props: ActionPopupEditProps) {
        super(props);
        this.state = {
            currentAction: new ActionDTO(),
            isCurrentActionEveryday: false,
            frequencyLabel: "",
            editDialogOpen: false,
            startTimeString: "",
            frequencyString: ""
        };
    }

    render() {
        const { classes } = this.props;

        return (
            <React.Fragment>
                <Dialog
                    fullScreen={false}
                    open={this.state.editDialogOpen}
                    onClose={this.editActionDialog_onClose.bind(this)}
                    fullWidth={true}
                    className={classes.dialog}
                >
                    <DialogTitle id="responsive-dialog-title">{this.state.currentAction.description}</DialogTitle>
                    <DialogContent>
                        <DialogContentText>
                            {this.state.currentAction.technicalName}
                        </DialogContentText>
                        <Grid
                            container
                            spacing={1}
                        >
                            <Grid item xs={12} sm={12} md={12} lg={12}>
                                <Switch
                                    checked={this.state.currentAction.isActive}
                                    onChange={this.actionIsActive_onChange.bind(this)}
                                    color="primary"
                                    size="small"
                                    inputProps={{ 'aria-label': 'primary checkbox' }}
                                />
                            </Grid>
                            <Grid item xs={12} alignItems="flex-start" direction="row">
                                <TextField
                                    id="time"
                                    value={this.state.startTimeString}
                                    label="Godz. rozp."
                                    className={classes.timeField}
                                    type="time"
                                    onChange={this.startTime_onChange.bind(this)}
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                    inputProps={{
                                        step: 300, // 5 min
                                    }}
                                />
                                {!this.state.isCurrentActionEveryday &&
                                    <TextField
                                        id="time"
                                        value={this.state.frequencyString}
                                        label="Częstotliwość"
                                        className={classes.timeField}
                                        type="time"
                                        onChange={this.frequency_onChange.bind(this)}
                                        InputLabelProps={{
                                            shrink: true,
                                        }}
                                        inputProps={{
                                            step: 300, // 5 min
                                        }}
                                    />}
                                <TextField
                                    id="check"
                                    value={this.state.isCurrentActionEveryday}
                                    label={this.state.frequencyLabel}
                                    className={this.state.isCurrentActionEveryday ? classes.checkBoxFieldWide : classes.checkBoxField}
                                    type="checkbox"
                                    onChange={this.isCurrentActionEveryday_onChange.bind(this)}
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                    InputProps={{ disableUnderline: true }}
                                />
                            </Grid>
                        </Grid>

                    </DialogContent>
                    <DialogActions>
                        <Button autoFocus onClick={this.editActionDialog_onClose} color="primary">
                            Anuluj
                        </Button>
                        <Button onClick={this.action_onSave.bind(this)} color="primary" autoFocus>
                            Zapisz
                        </Button>
                    </DialogActions>
                </Dialog>
            </React.Fragment>
        );
    }

    editActionOpenDialog(action: ActionDTO) {
        console.log("action from child component", action);
        let isActionEveryday: boolean = action.actionFrequency.frequency === 24 * 60;
        let freqLabel: string = this.prepareFrequencyLabel(isActionEveryday);

        let startTimeStr = this.state.currentAction.actionFrequency ?
            DateTimeHelper.totalMinutsToTimeString(this.state.currentAction.actionFrequency.startTime) : "00:00";

        let frequencyStr = this.state.currentAction.actionFrequency ?
            DateTimeHelper.totalMinutsToTimeString(this.state.currentAction.actionFrequency.frequency) : "00:00";

        this.setState({
            editDialogOpen: true,
            currentAction: action,
            isCurrentActionEveryday: isActionEveryday,
            frequencyLabel: freqLabel,
            startTimeString: startTimeStr,
            frequencyString: frequencyStr
        });
    }

    editActionDialog_onClose = () => {
        this.setState({
            editDialogOpen: false,
        });
    }

    actionIsActive_onChange = (e: any) => {
        let currentAction = this.state.currentAction;
        currentAction.isActive = e.target.checked;
        this.setState({
            currentAction: currentAction,
        });
    }

    isCurrentActionEveryday_onChange = (e: any) => {
        let isEveryday: boolean = e.target.checked;
        let freqLabel: string = this.prepareFrequencyLabel(isEveryday);

        this.setState({
            isCurrentActionEveryday: isEveryday,
            frequencyLabel: freqLabel,
        });
    }

    frequency_onChange = (e: any) => {
        console.log("frequency", e.target.value, DateTimeHelper.timeStringToTotalMinutes(e.target.value));

        let currentActionTemp = this.state.currentAction;
        currentActionTemp.actionFrequency.frequency = DateTimeHelper.timeStringToTotalMinutes(e.target.value);

        this.setState({
            frequencyString: e.target.value,
            currentAction: currentActionTemp
        });
    }

    startTime_onChange = (e: any) => {
        console.log("start time", e.target.value, DateTimeHelper.timeStringToTotalMinutes(e.target.value));

        let currentActionTemp = this.state.currentAction;
        currentActionTemp.actionFrequency.startTime = DateTimeHelper.timeStringToTotalMinutes(e.target.value);


        this.setState({
            startTimeString: e.target.value,
            currentAction: currentActionTemp
        });
    }

    action_onSave = () => {
        let action = this.state.currentAction;
        action.actionFrequency.frequency = DateTimeHelper.timeStringToTotalMinutes(this.state.frequencyString);
        action.actionFrequency.startTime = DateTimeHelper.timeStringToTotalMinutes(this.state.startTimeString);

        console.log("onSave", action);

        fetch(AppUrl.PostPeriodAction, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(action)
        }).then(() => {
            this.setState({
                editDialogOpen: false,
            });
            this.props.onUpdateActions();
        })
    }

    private prepareFrequencyLabel(isActionEveryday: boolean): string {
        let freqLabel: string = "";
        if (isActionEveryday) {
            freqLabel = "Częstotliwosć: co 24 h";
        } else {
            freqLabel = "co 24 h"
        }

        return freqLabel;
    }
}

const styles = (theme: any) => createStyles({
    root: {
        width: '100%',
    },
    heading: {
        fontSize: theme.typography.pxToRem(15),
        fontWeight: theme.typography.fontWeightRegular,
    },
    timeField: {
        marginLeft: theme.spacing(1),
        marginRight: theme.spacing(1),
        width: 85,
    },
    checkBoxField: {
        marginLeft: theme.spacing(1),
        marginRight: theme.spacing(1),
        height: 49,
        width: 40,
    },
    checkBoxFieldWide: {
        marginLeft: theme.spacing(1),
        marginRight: theme.spacing(1),
        width: 170,
        height: 49,
    },
    dialog: {
        margin: "0px"
    }
});

export default withStyles(styles)(ActionPopupEdit);