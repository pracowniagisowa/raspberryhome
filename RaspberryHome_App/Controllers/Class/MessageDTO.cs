namespace RaspberryHome_App.Controllers.Class
{
    public class MessageDTO
    {
        public MessageDTO(string message)
        {
            this.Message = message;
        }
        public string Message { get; set; }
    }
}