import { Card, createStyles, Grid, IconButton, TextField, Typography, WithStyles, withStyles } from "@material-ui/core";
import { Edit } from "@material-ui/icons";
import React from "react";
import { AppUrl } from "../../AppConstans";


export interface ThermometerFullInfoProps extends WithStyles<typeof styles> {
    thermometerFullInfo: any,
    idx: number,
    token: string,
}

export interface ThermometerFullInfoState {
    name: string,
    isEditable: boolean,
}

class ThermometerFullInfo extends React.Component<ThermometerFullInfoProps, ThermometerFullInfoState> {
    constructor(props: ThermometerFullInfoProps) {
        super(props);
        this.state = {
            name: this.props.thermometerFullInfo.name,
            isEditable: false,
        };
    }
    render() {
        return (
            <React.Fragment>
                <Card variant="outlined">
                    <Grid
                        container
                        spacing={1}>
                        <Grid item xs={1} sm={1} md={1} lg={1}>
                            <TextField
                                type="text"
                                margin="dense"
                                disabled={true}
                                inputProps={{ style: { textAlign: 'right' } }}
                                value={this.props.idx + "."}
                            />

                        </Grid>
                        <Grid item xs={6} sm={5} md={4} lg={3}>
                            <TextField
                                id="name"
                                margin="dense"
                                value={this.state.name}
                                disabled={!this.state.isEditable}

                                onChange={this.name_onChange.bind(this)} />
                        </Grid>
                        <Grid
                            item xs={3} sm={2} md={1} lg={1}
                        >
                            <TextField
                                type="text"
                                margin="dense"
                                disabled={true}
                                inputProps={{ style: { textAlign: 'right' } }}
                                value={this.props.thermometerFullInfo.temperature + " C"}
                            />
                        </Grid>
                        <Grid
                            item xs={2} sm={2} md={1} lg={1}>
                            <IconButton
                                onClick={this.thermometerFullInfo_onClick.bind(this)}
                            >
                                <Edit />
                            </IconButton>
                        </Grid>

                    </Grid>

                </Card>
            </React.Fragment>
        );
    }

    name_onChange = (e: any) => {
        var newName = e.target.value;
        console.log(newName);
        this.setState({
            name: newName,
        });

        let url = AppUrl.PutThermometerNameByThermometerID + "?" + new URLSearchParams({
            thermId: this.props.thermometerFullInfo.thermometerID,
            name: newName
        });
        console.log("url", url);

        fetch(url, { // "?thermId=" + 
            method: "PUT",
            headers: new Headers({
                Authorization:
                    "Bearer " + this.props.token
            }),
        })
    }

    thermometerFullInfo_onClick = (e: any) => {
        this.setState({
            isEditable: !this.state.isEditable,
        })
    }
}

const styles = (theme: any) => createStyles({

});

export default withStyles(styles)(ThermometerFullInfo);