﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace raspberryhome.Migrations
{
    public partial class GardenModule : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "SoilHumidityHistory",
                columns: table => new
                {
                    SoilHumidityId = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    IsHumid = table.Column<bool>(nullable: false),
                    DateTime = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SoilHumidityHistory", x => x.SoilHumidityId);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "SoilHumidityHistory");
        }
    }
}
