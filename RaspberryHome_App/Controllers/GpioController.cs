using System;
using System.Collections.Generic;
using System.Device.Gpio;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using RaspberryHome_App.Controllers.Class;
using RaspberryHome_App.DatabaseModels;
using RaspberryHome_App.Services;

namespace RaspberryHome_App.Controllers
{

    [ApiController]
    [Route("[controller]/[action]")]
    public class GpioController : ControllerBase
    {
        private readonly HomeDbContext _dbContext;
        private readonly IConfiguration _configuration;
        private IGpioManager _gpioManager;
        private readonly ILogger _logger;
        public GpioController(HomeDbContext dbContext, IGpioManager gpioManager, IConfiguration configuration, ILogger<GpioController> logger)
        {
            _dbContext = dbContext;
            _configuration = configuration;
            _logger = logger;
            _gpioManager = gpioManager;//new GpioManager(gpioController, configuration, _logger);
        }

        [HttpGet]
        public ActionResult GetSettings()
        {
            var settings = _dbContext.SettingsDb.ToList();
            return Ok(settings);
        }

        [HttpGet]
        public ActionResult PostSettingsDb([FromBody] List<SettingDb> settings)
        {
            var settingService = new SettingsService(_dbContext);
            foreach (var setting in settings)
            {

            }
            return Ok();
        }

        [HttpGet]
        public ActionResult SwichPinValue(int pinID)
        {
            try
            {
                _gpioManager.SwichPinValue(pinID, out string message);
                MessageDTO messageDTO = new MessageDTO(message);
                return Ok(messageDTO);
            }
            catch (System.Exception)
            {

                throw;
            }
        }

        [HttpGet]
        public ActionResult SetValueHighAtPin(int pinID)
        {
            try
            {
                _gpioManager.SetPinState(pinID, GpioPinValue.High);
                return Ok($"Set pin {pinID} to Value.High");

            }
            catch (System.Exception)
            {
                throw;
            }
        }

        [HttpGet]
        public ActionResult SetValuelowAtPin(int pinID)
        {
            try
            {
                _gpioManager.SetPinState(pinID, GpioPinValue.Low);

                return Ok($"Set pin {pinID} to Value.Low");
            }
            catch (System.Exception)
            {
                throw;
            }
        }

        [HttpGet]
        public ActionResult GetPinValue(int pinID)
        {
            var pinValue = _gpioManager.GetPinValue(pinID);
            _logger.LogInformation(pinValue.ToString());
            if (pinValue == GpioPinValue.High)
            {
                return Ok($"Pin {pinID} value is High");
            }
            else if (pinValue == GpioPinValue.Low)
            {
                return Ok($"Pin {pinID} value is Low");
            }
            else
            {
                return Ok(pinValue.ToString());
            }
        }

        [HttpGet]
        public ActionResult GetPinsFromDb()
        {
            var pins = _dbContext.GpioPinStates.ToList();
            var pinsOrdered = pins.OrderBy(x => x.PinNo).ToList();
            return Ok(pinsOrdered);
        }

        // public ActionResult IsPinOpen(int pinID)
        // {
        //     return Ok(_gpio.IsPinOpen(pinID));
        // }
    }
}