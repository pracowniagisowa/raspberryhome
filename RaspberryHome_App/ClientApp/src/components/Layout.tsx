import React from "react";
import { Route, Switch } from "react-router-dom";
import Dashboard from "./Dashboard";
import Home from "./DashboardComponents/HomeDashboard";
import { AppKey, AppPaths, AppUrl } from "../AppConstans";
import Login from "./Login";
import { useHistory } from "react-router-dom";
import { withRouter } from "react-router";
import HomeNavbar from "./HomeNavbar";
import GpioDashboard from "./DashboardComponents/GpioDashboard";
import Cookies from "universal-cookie";
import BaseComponent from "./BaseComponent";
import SettingsDashboard from "./DashboardComponents/SettingsDashboard";
import { Container } from "@material-ui/core";

export interface LayoutProps {
  history: any;
}

export interface LayoutState {
  isAuthorized: boolean;
}

class Layout extends React.Component<any, LayoutState> {
  cookies: Cookies;
  timerID: any;
  token: string;

  constructor(props: any) {
    super(props);

    this.state = {
      isAuthorized: false,
    };

    this.cookies = new Cookies();
    this.checkIsAuthorized();
  }

  render() {
    // console.log("Login render");
    return (
      <Container>
        {this.props.history.location.pathname !== AppPaths.UnauthorizedPath && (
          <HomeNavbar />
        )}
        <Switch>
          <Route path={AppPaths.UnauthorizedPath} component={Login}></Route>
          <Route exact path={AppPaths.HomePath} component={Home}></Route>
          <Route path={AppPaths.GpioPath} component={GpioDashboard}></Route>
          <Route path={AppPaths.SettingsPath} component={SettingsDashboard}></Route>
        </Switch>
      </Container>
    );
  }

  componentDidMount() {
    this.setState({ isAuthorized: true });
    // console.log("History:", this.props.history);

    this.timerID = setInterval(
      () => this.checkIsAuthorized(),
      600000
    );
  }
  componentWillUnmount() {
    console.log("Layout unmount");
    clearInterval(this.timerID);
  }

  checkIsAuthorized() {
    // console.log("checkIsAuthorized");
    // console.log("pathname", this.props.location.pathname);


    this.token = this.cookies.get(AppKey.cookieToken);
    // console.log(token);
    if (false) {
      this.props.history.push(AppPaths.HomePath);
    } else {
      fetch(AppUrl.GetUser, {
        method: "GET",
        headers: new Headers({
          Authorization:
            "Bearer " + this.token,
          //"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJmODkwNmMxZi1hYjAzLTQxM2UtODNjZC0wMmQ3YWUzYTY5YjUiLCJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9uYW1lIjoiamtvd2Fsc2tpZSIsImp0aSI6IjA3MzlhYzRmLWFiMmEtNDhkYS1hZmEwLWFjOWYyMjQzYzFlZSIsImh0dHA6Ly9zY2hlbWFzLnhtbHNvYXAub3JnL3dzLzIwMDUvMDUvaWRlbnRpdHkvY2xhaW1zL25hbWVpZGVudGlmaWVyIjoiZjg5MDZjMWYtYWIwMy00MTNlLTgzY2QtMDJkN2FlM2E2OWI1IiwiZXhwIjoxNjA0MTYyMTA1LCJpc3MiOiJodHRwOi8vbG9jYWxob3N0OjUwMDAiLCJhdWQiOiJodHRwOi8vbG9jYWxob3N0OjUwMDAifQ.SNAKe3PD_4e6CkF5q5-87cPhF5jPGLHsb4msmvFOLKg",
        }),
      })
        .then((resp) => resp.json())
        .then(() => this.redirect(true))
        .catch((e) => this.redirect(false));
    }
  }

  redirect(isAuthorized: boolean) {
    if (isAuthorized) {
      if (this.props.location.pathname === AppPaths.UnauthorizedPath) {
        this.props.history.push(AppPaths.HomePath);
      }
    } else {
      this.props.history.push(AppPaths.UnauthorizedPath);
    }
  }
}

export default withRouter(Layout);
// export default Layout;
