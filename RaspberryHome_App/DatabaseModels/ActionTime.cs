using System;
using System.ComponentModel.DataAnnotations;

namespace RaspberryHome_App.DatabaseModels
{
    public class ActionTime
    {
        [Key]
        public int ActionTimeId { get; set; }
        public int TimeTotalMinutes { get; set; }
        public bool IsActive { get; set; }
    }
}