using System;

namespace RaspberryHome_App.Services.Weather.Classes
{
    public class FrostInADay
    {
        public DateTime DateTime { get; set; }
        public double Temperature { get; set; }

        public FrostInADay(DateTime dateTime, double temperature)
        {
            DateTime = dateTime;
            Temperature = temperature;
        }

        public override string ToString()
        {
            return $"{DateTime.TimeOfDay} - {Temperature} C {Environment.NewLine}";
        }
    }
}