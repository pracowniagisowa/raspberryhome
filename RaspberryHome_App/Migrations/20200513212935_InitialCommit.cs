﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace raspberryhome.Migrations
{
    public partial class InitialCommit : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ActionFrequency",
                columns: table => new
                {
                    ActionFrequencyId = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    StartTime = table.Column<int>(nullable: false),
                    Frequency = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ActionFrequency", x => x.ActionFrequencyId);
                });

            migrationBuilder.CreateTable(
                name: "GpioPinStates",
                columns: table => new
                {
                    GpioStateId = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    PinPhisicalNo = table.Column<int>(nullable: false),
                    PinNo = table.Column<int>(nullable: false),
                    IsOpen = table.Column<bool>(nullable: false),
                    PinMode = table.Column<int>(nullable: false),
                    IsStateHigh = table.Column<bool>(nullable: false),
                    Comment = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GpioPinStates", x => x.GpioStateId);
                });

            migrationBuilder.CreateTable(
                name: "PeriodActionHistories",
                columns: table => new
                {
                    PeriodActionHistoryId = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    TechnicalName = table.Column<string>(nullable: true),
                    InitTime = table.Column<DateTime>(nullable: false),
                    IsActionTriggered = table.Column<bool>(nullable: false),
                    Comment = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PeriodActionHistories", x => x.PeriodActionHistoryId);
                });

            migrationBuilder.CreateTable(
                name: "TestEntities",
                columns: table => new
                {
                    TestEntityId = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Comment = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TestEntities", x => x.TestEntityId);
                });

            migrationBuilder.CreateTable(
                name: "PeriodActions",
                columns: table => new
                {
                    PeriodActionId = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    TechnicalName = table.Column<string>(nullable: true),
                    ActionFrequencyId = table.Column<int>(nullable: true),
                    Description = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PeriodActions", x => x.PeriodActionId);
                    table.ForeignKey(
                        name: "FK_PeriodActions_ActionFrequency_ActionFrequencyId",
                        column: x => x.ActionFrequencyId,
                        principalTable: "ActionFrequency",
                        principalColumn: "ActionFrequencyId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ActionTime",
                columns: table => new
                {
                    ActionTimeId = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    TimeTotalMinutes = table.Column<int>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    PeriodActionId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ActionTime", x => x.ActionTimeId);
                    table.ForeignKey(
                        name: "FK_ActionTime_PeriodActions_PeriodActionId",
                        column: x => x.PeriodActionId,
                        principalTable: "PeriodActions",
                        principalColumn: "PeriodActionId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ActionTime_PeriodActionId",
                table: "ActionTime",
                column: "PeriodActionId");

            migrationBuilder.CreateIndex(
                name: "IX_PeriodActions_ActionFrequencyId",
                table: "PeriodActions",
                column: "ActionFrequencyId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ActionTime");

            migrationBuilder.DropTable(
                name: "GpioPinStates");

            migrationBuilder.DropTable(
                name: "PeriodActionHistories");

            migrationBuilder.DropTable(
                name: "TestEntities");

            migrationBuilder.DropTable(
                name: "PeriodActions");

            migrationBuilder.DropTable(
                name: "ActionFrequency");
        }
    }
}
