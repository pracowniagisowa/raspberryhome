import RPi.GPIO as GPIO
import time
pin_id = 23

GPIO.setmode(GPIO.BCM)
GPIO.setup(pin_id, GPIO.OUT)

while True:
    GPIO.output(pin_id, GPIO.HIGH)
    print("set high")
    time.sleep(1)
    GPIO.output(pin_id, GPIO.LOW)
    print("low")
    time.sleep(1)