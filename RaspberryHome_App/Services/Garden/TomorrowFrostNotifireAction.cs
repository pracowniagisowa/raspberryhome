using System;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using RaspberryHome_App.DatabaseModels;
using RaspberryHome_App.Services.Weather;
using RaspberryHome_App.SideServices;
using RaspberryHome_App.SideServices.Classes;

namespace RaspberryHome_App.Services.Garden
{
    public class TomorrowFrostNotifireAction : BaseAction
    {
        public TomorrowFrostNotifireAction()
        {
        }

        public TomorrowFrostNotifireAction(
            IServiceScopeFactory scopeFactory,
            ILogger logger)
        : base(scopeFactory, logger)
        {
        }

        public override ActionResponse Action()
        {
            ActionResponse response = new ActionResponse();
            response.Message = String.Empty;

            var weatherManager = new OpenWeatherMapService(); //TomorrowFros
            var tomorrowFrost = weatherManager.TomorrowFrost();
            if (tomorrowFrost.Count > 0)
            {
                response.Triggered = true;
                string weatherMessage = string.Join(" ", tomorrowFrost); //String.Empty;

                using (var scope = _scopeFactory.CreateScope())
                {
                    var emailSerivce = scope.ServiceProvider.GetRequiredService<EmailService>();
                    var task = emailSerivce.SendEmail(
                        emailAdress: "maciek.fertala@gmail.com",
                        subject: "Jutro będzie przymrozek",
                        body: weatherMessage,
                        isBodyHtml: false
                    );
                    task.Wait();

                    response.Success = true;
                    response.Triggered = true;
                }
            }


            return response;

        }
    }
}