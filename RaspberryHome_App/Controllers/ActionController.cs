using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using RaspberryHome_App.DatabaseModels;

namespace RaspberryHome_App.Controllers
{
    [ApiController]
    [Route("[controller]/[action]")]
    public class ActionController : ControllerBase
    {
        private readonly HomeDbContext _dbContext;
        private readonly IConfiguration _configuration;
        private readonly ILogger<ActionController> _logger;

        public ActionController(HomeDbContext dbContext, IConfiguration configuration, ILogger<ActionController> logger)
        {
            _dbContext = dbContext;
            _configuration = configuration;
            _logger = logger;
        }

        [HttpPost]
        public ActionResult PostPeriodAction([FromBody] PeriodAction periodAction)
        {
            var periodActions = _dbContext.PeriodActions
                                    .Include(x => x.ActionFrequency)
                                    .Include(x => x.ActionTimes);
            var periodActionDb = periodActions.FirstOrDefault(x => x.TechnicalName == periodAction.TechnicalName);
            if (periodActionDb == null)
            {
                _dbContext.PeriodActions.Add(periodAction);
            }
            else
            {
                periodActionDb.Description = periodAction.Description;
                if (periodAction.ActionFrequency != null)
                {
                    periodActionDb.ActionFrequency.Frequency = periodAction.ActionFrequency.Frequency;
                    periodActionDb.ActionFrequency.StartTime = periodAction.ActionFrequency.StartTime;
                }
                // jeszcze obsluzyc interwaly

            }


            _dbContext.SaveChanges();
            return Ok();
        }

        [HttpGet]
        public ActionResult GetActions()
        {
            var periodActions = _dbContext.PeriodActions
                                    .Include(x => x.ActionFrequency)
                                    .Include(x => x.ActionTimes);
            return Ok(periodActions);
        }

        [HttpPut]
        public ActionResult SetActionActive([FromQuery] int actionID, [FromQuery] bool isActive)
        {
            var periodAction = _dbContext.PeriodActions.FirstOrDefault(x => x.PeriodActionId == actionID);
            periodAction.IsActive = isActive;
            _dbContext.SaveChanges();
            return Ok();
        }
    }
}