using System;

namespace RaspberryHome_App.DTOs
{
    public class ResponseDTO
    {
        public Guid ID { get; set; }
        public bool Success { get; set; }
        public string Message { get; set; }
    }
}