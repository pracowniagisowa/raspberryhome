using System.Device.Gpio;
using System;

namespace RPI_GPIO_Tests
{
    public class DeviceGpioHelper : IGpioHelper
    {
        GpioController _gpio;
        public DeviceGpioHelper()
        {
            _gpio = new GpioController();
        }

        public void SetPinState(int pinId, int pinValue, bool saveToDb = true)
        {
            Console.WriteLine($"Seting pin {pinId} state");
            if (!_gpio.IsPinOpen(pinId))
            {
                Console.WriteLine($"Pin {pinId} not open.");
                _gpio.OpenPin(pinId, PinMode.Output);

                Console.WriteLine($"Is pin {pinId} open: {_gpio.IsPinOpen(pinId)}");
            }
            // Console.Writeline($"Pin {pinId} open??");
            // gpio.OpenPin(pinId, PinMode.Output);
            // gpio.SetPinMode(pinId, PinMode.Output);
            // Console.Writeline($"Pin {pinId} open for OUTPUT");

            // zastanowić się co zrobić jak jest otwarty, ale na input

            _gpio.Write(pinId, pinValue);
            Console.WriteLine($"Pin {pinId} state set to {pinValue.ToString()}");
        }
    }
}