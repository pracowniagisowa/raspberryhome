# RaspberryHome App

The purpose of this application is to test the Raspberry Pi's ability as a smart home server.

### Technologies

- .NET Core + React (TypeScript)
- Entity Framework
- Identity

### FrontEnd

- Home dashboard:

![ScreenShot](https://bitbucket.org/pracowniagisowa/raspberryhome/raw/c695e940a7876d446e5b45d8e3d28ed30130b0aa/screens/home.PNG)

- Settings:

![ScreenShot](https://bitbucket.org/pracowniagisowa/raspberryhome/raw/c695e940a7876d446e5b45d8e3d28ed30130b0aa/screens/settings.PNG)

- GPIO dashboard:

![ScreenShot](https://bitbucket.org/pracowniagisowa/raspberryhome/raw/c695e940a7876d446e5b45d8e3d28ed30130b0aa/screens/gpio.PNG)
