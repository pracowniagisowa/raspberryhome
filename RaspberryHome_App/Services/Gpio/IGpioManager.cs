// using System.Device.Gpio;

namespace RaspberryHome_App.Services
{
    public interface IGpioManager
    {
        void InitGpio();

        void SetPinState(int pinId, GpioPinValue pinValue, bool saveToDb = true);

        GpioPinValue GetPinValue(int pinId, bool saveToDb = true);

        bool SwichPinValue(int pinId, out string message);

        bool IsPinStateHighDb(int pinID);

    }
}