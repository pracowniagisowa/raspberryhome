import React from "react";
import Navbar from "react-bootstrap/Navbar";
import Nav from "react-bootstrap/Nav";
import DropdownButton from "react-bootstrap/DropdownButton";
import Dropdown from "react-bootstrap/Dropdown";
import { BrowserRouter as Router, Link, withRouter } from "react-router-dom";
import { AppPaths } from "../AppConstans";
import { AppBar, createStyles, IconButton, Menu, MenuItem, Tab, Tabs, Toolbar, Typography, WithStyles, withStyles } from "@material-ui/core";
import { compose } from "recompose";
import clsx from "clsx";
import { AccountCircle } from "@material-ui/icons";
import classes from "*.module.sass";

export interface HomeNavbarProps extends WithStyles<typeof styles> { }

export interface HomeNavbarState {
  tabValue: number;
}

class HomeNavbar extends React.Component<any, HomeNavbarState> {
  tabs: TabProps[];

  constructor(props: any) {
    super(props);
    this.state = {
      tabValue: 0,
    }

    this.tabs = new Array<TabProps>();
    this.tabs.push(new TabProps("Home", AppPaths.HomePath));
    this.tabs.push(new TabProps("GPIO", AppPaths.GpioPath));
    this.tabs.push(new TabProps("Ustawienia", AppPaths.SettingsPath));

  }

  componentDidMount() {
    var tabValue = this.getTabValue();
    console.log("tabValue", tabValue);
    this.setState({
      tabValue: tabValue
    });
  }


  render() {
    const { classes } = this.props;

    return (
      <div className={classes.grow}>
        <AppBar
          position="static"
          className={clsx(classes.appBar)} >
          <Toolbar>
            <Typography variant="h6" className={classes.title} noWrap>
              Raspberry Home
            </Typography>
            <Tabs value={this.state.tabValue} onChange={this.handleChange}>
              {this.tabs.map((x, index) =>
                <Tab key={index} label={x.label} to={x.to} component={Link} />)}
            </Tabs>
            <div className={classes.grow} />

            <div>
              <IconButton
                aria-label="account of current user"
                aria-controls="menu-appbar"
                aria-haspopup="true"
                color="inherit"
              >
                <AccountCircle />
              </IconButton>

            </div>
          </Toolbar>

        </AppBar>
      </div>
    );
  }

  getTabValue() {
    var currentPath = this.props.location.pathname;
    console.log("Navbar current path", currentPath);
    console.log(this.tabs, "length", this.tabs.length);
    var tabValue = 0;
    for (let index = 0; index < this.tabs.length; index++) {
      var tabProps = this.tabs[index];
      if (tabProps.to === currentPath) {
        console.log("i", index, tabProps)
        tabValue = index;
        break;
      }

    }

    return tabValue;
  }

  handleChange = (event: React.ChangeEvent<{}>, newValue: number) => {
    console.log('value', newValue);
    this.setState({
      tabValue: newValue,
    });
  };
}

export class TabProps {
  label: string;
  to: string;
  component: any;

  constructor(label: string, to: string) {
    this.label = label;
    this.to = to;
  }
}

const drawerWidth = 240;

const styles = (theme: any) => createStyles({
  grow: {
    flexGrow: 1,
  },
  root: {
    display: 'flex',
  },
  toolbar: {
    paddingRight: 24, // keep right padding when drawer closed
  },
  toolbarIcon: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: '0 8px',
    ...theme.mixins.toolbar,
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  appBarShift: {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  menuButton: {
    marginRight: 36,
  },
  menuButtonHidden: {
    display: 'none',
  },
  title: {
    display: 'none',
    [theme.breakpoints.up('sm')]: {
      display: 'block',
    },
  },
  drawerPaper: {
    position: 'relative',
    whiteSpace: 'nowrap',
    width: drawerWidth,
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  drawerPaperClose: {
    overflowX: 'hidden',
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    width: theme.spacing(7),
    [theme.breakpoints.up('sm')]: {
      width: theme.spacing(9),
    },
  },
  appBarSpacer: theme.mixins.toolbar,
  content: {
    flexGrow: 1,
    height: '100vh',
    overflow: 'auto',
  },
  container: {
    paddingTop: theme.spacing(4),
    paddingBottom: theme.spacing(4),
  },
  paper: {
    padding: theme.spacing(2),
    display: 'flex',
    overflow: 'auto',
    flexDirection: 'column',
  },
  fixedHeight: {
    height: 240,
  },
});



export default compose(
  withRouter,
  withStyles(styles))
  (HomeNavbar);

