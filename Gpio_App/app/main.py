from fastapi import FastAPI
import RPi.GPIO as GPIO

app = FastAPI()

@app.get("/")
async def root():
    return {"message": "Hello World"}

@app.get("/set-pin-high/{pin_id}")
async def set_pin_high(pin_id: int):
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(pin_id , GPIO.OUT)
    GPIO.output(pin_id , GPIO.HIGH)

@app.get("/set-pin-low/{pin_id}")
async def set_pin_low(pin_id: int):
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(pin_id , GPIO.OUT)
    GPIO.output(pin_id , GPIO.LOW)

@app.get("/get-pin-value/{pin_id}")
async def get_pin_value(pin_id: int):
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(pin_id , GPIO.IN)
    return GPIO.input(pin_id)

@app.get("/set-pin-test/{pin_id}")
async def set_pin_test(pin_id: int):
    return {"pin_id": pin_id}

