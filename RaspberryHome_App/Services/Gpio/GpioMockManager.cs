using System;
using System.Collections.Generic;
// using System.Device.Gpio;
using System.Linq;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using RaspberryHome_App.DatabaseModels;

namespace RaspberryHome_App.Services
{
    public class GpioMockManager : IGpioManager
    {
        // private readonly GpioController _gpio;
        // private readonly IConfiguration _configuration
        private readonly ILogger<GpioMockManager> _logger;
        private readonly IServiceScopeFactory _scopeFactory;

        public GpioMockManager(IServiceScopeFactory scopeFactory, ILogger<GpioMockManager> logger)
        {
            // _gpio = gpioController;
            // _configuration = configuration;
            _scopeFactory = scopeFactory;
            _logger = logger;
        }

        public void InitGpio()
        {
            _logger.LogInformation("GPIO initialising");
            List<GpioPinState> pinStates = new List<GpioPinState>();
            using (var scope = _scopeFactory.CreateScope())
            {
                var dbContext = scope.ServiceProvider.GetRequiredService<HomeDbContext>();
                pinStates = dbContext.GpioPinStates.ToList();
            }
            foreach (var pin in pinStates)
            {
                if (pin.IsStateHigh)
                {
                    // SetPinState(pin.PinNo, PinValue.High, false);
                }
                else
                {
                    // SetPinState(pin.PinNo, PinValue.Low, false);
                }
            }
            _logger.LogInformation("GPIO initialised");

        }

        public void SetPinState(int pinId, GpioPinValue pinValue, bool saveToDb = true)
        {
            // if (!_gpio.IsPinOpen(pinId))
            // {
            //     _gpio.OpenPin(pinId, PinMode.Output);
            // }
            // zastanowić się co zrobić jak jest otwarty, ale na input
            // _gpio.Write(pinId, pinValue);
            _logger.LogInformation($"Pin {pinId} state set to {pinValue.ToString()}");
            if (saveToDb)
            {
                SavePinState(pinId, GpioPinMode.Output, pinValue);
            }
        }

        public GpioPinValue GetPinValue(int pinId, bool saveToDb = true)
        {
            // if (!_gpio.IsPinOpen(pinId))
            // {
            //     _gpio.OpenPin(pinId, PinMode.Input);
            // }
            // var value = _gpio.Read(pinId);
            // _gpio.ClosePin(pinId);
            return GpioPinValue.High;
        }

        private void SavePinState(int pinId, GpioPinMode pinMode, GpioPinValue pinValue)
        {
            using (var scope = _scopeFactory.CreateScope())
            {
                var dbContext = scope.ServiceProvider.GetRequiredService<HomeDbContext>();
                var pinState = dbContext.GpioPinStates.FirstOrDefault(x => x.PinNo == pinId);
                if (pinState == null)
                {
                    pinState = new GpioPinState(pinId);
                    pinState.IsOpen = true;
                    pinState.PinMode = pinMode;
                    pinState.IsStateHigh = pinValue == GpioPinValue.High;
                    dbContext.GpioPinStates.Add(pinState);

                }
                else
                {
                    pinState.IsOpen = true;
                    pinState.PinMode = pinMode;
                    pinState.IsStateHigh = pinValue == GpioPinValue.High;
                }
                dbContext.SaveChanges();
            }
        }

        public bool SwichPinValue(int pinId, out string message)
        {
            message = String.Empty;
            try
            {
                if (IsPinStateHighDb(pinId))
                {
                    SetPinState(pinId, GpioPinValue.Low);
                    message += 0.ToString();
                }
                else
                {
                    SetPinState(pinId, GpioPinValue.High);
                    message += 1.ToString();
                }
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
            return true;
        }

        public bool IsPinStateHighDb(int pinID)
        {
            using (var scope = _scopeFactory.CreateScope())
            {
                var dbContext = scope.ServiceProvider.GetRequiredService<HomeDbContext>();
                var pinState = dbContext.GpioPinStates.FirstOrDefault(x => x.PinNo == pinID);
                if (pinState == null)
                {
                    return false;
                }
                return pinState.IsStateHigh;
            }
        }
    }
}