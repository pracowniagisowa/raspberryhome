import React from "react";
import { withRouter } from "react-router-dom";
import Cookies from "universal-cookie";
import { AppKey } from "../AppConstans";
 
class BaseComponent extends React.Component {

    public cookies: Cookies;
    public token: string;

    constructor(props: any) {
        super(props);
        this.state = {};

        this.cookies = new Cookies();
        this.token = this.cookies.get(AppKey.cookieToken);
    }

    render(){
        return (<div></div>)
    }

    componentDidMount(){
        console.log("BaseComponent did mount");
    }

    
}
 
export default (BaseComponent);