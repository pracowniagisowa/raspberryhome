using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using RaspberryHome_App.SideServices;
using RaspberryHome_App.SideServices.Classes;

namespace RaspberryHome_App.Services.Garden
{
    public class DryHomePlantsNotifireAction : BaseAction
    {
        // private readonly GardenManager _gardenManager;
        public DryHomePlantsNotifireAction()
        {
        }

        public DryHomePlantsNotifireAction(
            IServiceScopeFactory scopeFactory,
            ILogger logger)
        : base(scopeFactory, logger)
        {
            // _gardenManager = gardenManager;
        }

        public override ActionResponse Action()
        {
            ActionResponse resp = new ActionResponse();
            using (var scope = _scopeFactory.CreateScope())
            {
                var gardenManager = scope.ServiceProvider.GetRequiredService<GardenManager>();
                var isSoilHumid = gardenManager.IsSoilHumid((int)GpioPinsUsage.SoilHumidityWindowsillSensorValue, (int)GpioPinsUsage.SoilHumidityWindowsillSensorPower);
                if (!isSoilHumid)
                {
                    var emailManager = scope.ServiceProvider.GetRequiredService<EmailService>();
                    var task = emailManager.SendEmail(
                        emailAdress: "maciek.fertala@gmail.com",
                        subject: "Podlewanie",
                        body: "Podlej rośliny w domu",
                        isBodyHtml: false
                    );
                    task.Wait();
                    resp.Success = true;
                    resp.Triggered = true;
                }
                else
                {
                    resp.Success = true;
                    resp.Triggered = false;
                }
            }

            return resp;
        }


    }
}