using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using RaspberryHome_App.DatabaseModels;
using RaspberryHome_App.Services;

namespace RaspberryHome_App.Controllers
{
    [ApiController]
    [Route("[controller]/[action]")]
    public class TestsController : ControllerBase
    {
        private readonly HomeDbContext _dbContext;
        private readonly EmailService _emailService;
        private readonly ILogger<TestsController> _logger;

        public TestsController(
            HomeDbContext dbContext,
            EmailService emailService,
            ILogger<TestsController> logger)
        {
            _dbContext = dbContext;
            _emailService = emailService;
            _logger = logger;
        }

        [HttpGet]
        public ActionResult DbTest()
        {

            _logger.LogInformation("no ot cyk!!");
            var test = new TestEntity();
            DateTime now = DateTime.Now;
            test.Comment = now.ToString("yyyy-MM-dd HH:mm:ss");
            _dbContext.TestEntities.Add(test);
            _dbContext.SaveChanges();
            return Ok();
        }

        [HttpGet]
        public async Task<ActionResult> SendEmail()
        {

            await _emailService.SendEmail(
                        emailAdress: "maciek.fertala@gmail.com",
                        subject: "Raspberry Test",
                        body: "test test test test test test test test test",
                        isBodyHtml: false
                    );
            return Ok();
        }
    }
}