using System;
using System.Device.Gpio;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using RaspberryHome_App.DatabaseModels;
using RaspberryHome_App.DatabaseModels.Garden;
using RaspberryHome_App.SideServices;
using RaspberryHome_App.SideServices.Classes;

namespace RaspberryHome_App.Services.Garden
{
    public class SoilHumidityToDbAction : BaseAction
    {
        // private readonly GardenManager _gardenManager;
        // private readonly IServiceScopeFactory _scopeFactory;

        public SoilHumidityToDbAction()
        {
        }

        public SoilHumidityToDbAction(
            IServiceScopeFactory scopeFactory,
            // GardenManager gardenManager,
            ILogger logger)
            : base(scopeFactory, logger)
        {
            // _scopeFactory = scopeFactory;
            // _dbContext = dbContext;
            // _gardenManager = gardenManager;
        }

        public override ActionResponse Action()
        {
            ActionResponse response = new ActionResponse();
            response.Triggered = true;
            response.Message = String.Empty;

            using (var scope = _scopeFactory.CreateScope())
            {
                var gardenManager = scope.ServiceProvider.GetRequiredService<GardenManager>();
                SoilHumidity humidity = new SoilHumidity();
                humidity.IsHumid = gardenManager.IsSoilHumid((int)GpioPinsUsage.SoilHumidityBalconySensorValue, (int)GpioPinsUsage.SoilHumidityBalconySensorPower);

                var dbContext = scope.ServiceProvider.GetRequiredService<HomeDbContext>();
                dbContext.SoilHumidityHistory.Add(humidity);
                dbContext.SaveChanges();
            }


            response.Success = true;

            return response;
        }
    }
}