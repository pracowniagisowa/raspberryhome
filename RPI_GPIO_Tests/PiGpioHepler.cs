using System;
using Unosquare.RaspberryIO;
using Unosquare.RaspberryIO.Abstractions;
using Unosquare.WiringPi;

namespace RPI_GPIO_Tests
{
    public class PiGpioHepler : IGpioHelper
    {
        public PiGpioHepler()
        {
            Pi.Init<BootstrapWiringPi>();
        }

        public void SetPinState(int pinId, int pinValue, bool saveToDb = true)
        {
            var pin = Pi.Gpio[pinId];

            pin.PinMode = GpioPinDriveMode.Output;
            pin.Write(pinValue == 1);
            Console.WriteLine($"Pin {pinId} state set to {pinValue.ToString()}");


        }
    }
}