using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using RaspberryHome_App.Services;
using RaspberryHome_App.Services.Home;

namespace RaspberryHome_App.SideServices
{
    public class PinReader
    {
        private ILogger _logger;
        private readonly IServiceScopeFactory _scopeFactory;

        private GpioPinValue _testSwitchPinValue = GpioPinValue.Low;

        public PinReader(IServiceScopeFactory scopeFactory,
                            ILogger<TimeScheduler> logger)
        {
            _scopeFactory = scopeFactory;
            _logger = logger;
        }

        public void WatchPin()
        {
            using (var scope = _scopeFactory.CreateScope())
            {
                var gpioManager = scope.ServiceProvider.GetRequiredService<IGpioManager>();
                GpioPinValue pinValue = gpioManager.GetPinValue((int)GpioPinsUsage.TestSwitch, false);
                if (_testSwitchPinValue != pinValue)
                {
                    if (pinValue == GpioPinValue.High)
                    {
                        var action = new SwitchLightAction(_scopeFactory, _logger);
                        action.RunInBackground();
                    }

                    _testSwitchPinValue = pinValue;

                }
            }
        }
    }
}