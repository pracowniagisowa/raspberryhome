namespace RaspberryHome_App.DatabaseModels.Identity
{
    public class Credentials
    {
        public string UserName { get; set; }
        public string Name { get; set; }
        public string Password { get; set; }
        public string NewPassword { get; set; }
        
    }
}