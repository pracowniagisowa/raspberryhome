using System;
using System.Collections.Generic;
using System.Device.Gpio;
using System.Diagnostics;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using RaspberryHome_App.DatabaseModels;

namespace RaspberryHome_App.Services
{
    public class GpioManager : IGpioManager
    {
        private readonly GpioController _gpio;
        private readonly ILogger<GpioManager> _logger;
        private readonly IServiceScopeFactory _scopeFactory;

        public GpioManager(GpioController gpioController, IServiceScopeFactory scopeFactory, ILogger<GpioManager> logger)
        {
            _gpio = gpioController;
            _scopeFactory = scopeFactory;
            _logger = logger;
            _logger.LogInformation($"GpioManager: {_gpio}, {_gpio.NumberingScheme}");
        }

        public void InitGpio()
        {
            _logger.LogInformation("GPIO initialising");
            List<GpioPinState> pinStates = new List<GpioPinState>();
            using (var scope = _scopeFactory.CreateScope())
            {
                var dbContext = scope.ServiceProvider.GetRequiredService<HomeDbContext>();
                pinStates = dbContext.GpioPinStates.ToList();
            }
            foreach (var pin in pinStates)
            {
                if (pin.IsStateHigh)
                {
                    SetPinState(pin.PinNo, GpioPinValue.High, false);
                }
                else
                {
                    SetPinState(pin.PinNo, GpioPinValue.Low, false);
                }
            }
            _logger.LogInformation("GPIO initialised");

        }

        // public void SetPinState(int pinId, PinValue pinValue, bool saveToDb = true)
        // {
        //     _logger.LogInformation($"Seting pin {pinId} state");
        //     if (!_gpio.IsPinOpen(pinId))
        //     {
        //         _logger.LogInformation($"Pin {pinId} not open.");
        //         _gpio.OpenPin(pinId, PinMode.Output);
        //     }
        //     _logger.LogInformation($"Pin {pinId} open.");
        //     // zastanowić się co zrobić jak jest otwarty, ale na input
        //     _gpio.Write(pinId, pinValue);
        //     _logger.LogInformation($"Pin {pinId} state set to {pinValue.ToString()}");
        //     if (saveToDb)
        //     {
        //         SavePinState(pinId, PinMode.Output, pinValue);
        //     }
        // }

        public void SetPinState(int pinId, GpioPinValue pinValue, bool saveToDb = true)
        {
            _logger.LogInformation($"Seting pin {pinId} state");
            if (!_gpio.IsPinOpen(pinId))
            {
                _logger.LogInformation($"Pin {pinId} not open.");
                _gpio.OpenPin(pinId, PinMode.Output);

                _logger.LogInformation($"Is pin {pinId} open: {_gpio.IsPinOpen(pinId)}");
            }
            // _logger.LogInformation($"Pin {pinId} open??");
            // _gpio.OpenPin(pinId, PinMode.Output);
            // _gpio.SetPinMode(pinId, PinMode.Output);
            // _logger.LogInformation($"Pin {pinId} open for OUTPUT");

            // zastanowić się co zrobić jak jest otwarty, ale na input
            _gpio.Write(pinId, (int)pinValue);
            _logger.LogInformation($"Pin {pinId} state set to {pinValue.ToString()}");
            if (saveToDb)
            {
                SavePinState(pinId, GpioPinMode.Output, pinValue);
            }
        }

        public GpioPinValue GetPinValue(int pinId, bool saveToDb = true)
        {
            if (!_gpio.IsPinOpen(pinId))
            {
                _gpio.OpenPin(pinId, PinMode.Input);
            }
            var value = _gpio.Read(pinId);
            var ret = GpioPinValue.Low;
            if (value == PinValue.High)
            {
                ret = GpioPinValue.High;
            }
            else
            {
                ret = GpioPinValue.Low;
            }
            _gpio.ClosePin(pinId);
            return ret;
        }

        public bool SwichPinValue(int pinId, out string message)
        {
            message = String.Empty;
            try
            {
                if (IsPinStateHighDb(pinId))
                {
                    SetPinState(pinId, GpioPinValue.Low);
                    message += PinValue.Low.ToString();
                }
                else
                {
                    SetPinState(pinId, GpioPinValue.High);
                    message += PinValue.High.ToString();
                }
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
            return true;
        }

        public bool IsPinStateHighDb(int pinID)
        {
            using (var scope = _scopeFactory.CreateScope())
            {
                var dbContext = scope.ServiceProvider.GetRequiredService<HomeDbContext>();
                var pinState = dbContext.GpioPinStates.FirstOrDefault(x => x.PinNo == pinID);
                if (pinState == null)
                {
                    return false;
                }
                return pinState.IsStateHigh;
            }
        }

        private void SavePinState(int pinId, GpioPinMode pinMode, GpioPinValue pinValue)
        {
            using (var scope = _scopeFactory.CreateScope())
            {
                var dbContext = scope.ServiceProvider.GetRequiredService<HomeDbContext>();
                var pinState = dbContext.GpioPinStates.FirstOrDefault(x => x.PinNo == pinId);
                if (pinState == null)
                {
                    pinState = new GpioPinState(pinId);
                    pinState.IsOpen = true;
                    pinState.PinMode = pinMode;
                    pinState.IsStateHigh = pinValue == GpioPinValue.High;
                    dbContext.GpioPinStates.Add(pinState);

                }
                else
                {
                    pinState.IsOpen = true;
                    pinState.PinMode = pinMode;
                    pinState.IsStateHigh = pinValue == GpioPinValue.High;
                }
                dbContext.SaveChanges();
            }
        }

    }
}