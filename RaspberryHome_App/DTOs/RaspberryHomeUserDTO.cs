using System;

namespace RaspberryHome_App.DTOs
{
    public class RaspberryHomeUserDTO
    {
        public Guid Id { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
    }
}